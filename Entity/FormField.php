<?php

namespace PiZone\FormBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="pz_form_field")
 * @ORM\Entity(repositoryClass="PiZone\FormBundle\Entity\Repository\FormFieldRepository")
 */
class FormField {

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Form", inversedBy="form_fields")
     * @ORM\JoinColumn(name="form_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $form;
    
    /**
     * @ORM\ManyToOne(targetEntity="Field", inversedBy="form_fields")
     * @ORM\JoinColumn(name="form_field_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $field;
    
    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Please enter title.", groups={"FormField"})
     */
    protected $title;
    
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $class;
    
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $placeholder;
    
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $attr;
    
    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $is_required = false;
    
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $regexp_pattern;


    /**
     * @ORM\Column(type="string", length=100)
     * @Assert\NotBlank(message="Please enter alias.", groups={"FormField"})
     * @Assert\Regex( 
     *       pattern="/^[a-z,A-Z,\_,\-,0-9]+$/",
     *       message="Alias can contain only letters, numbers and symbols '_' , '-'.", 
     *       groups={"FormField"}
     * )
     */
    protected $alias;
    
    /**
    * @ORM\Column(type="integer", nullable=true)
     */
    protected $sort = 100;
    
    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $default_value;
    
    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $is_active = true;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return FormField
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set sort
     *
     * @param integer $sort
     * @return FormField
     */
    public function setSort($sort)
    {
        $this->sort = $sort;

        return $this;
    }

    /**
     * Get sort
     *
     * @return integer 
     */
    public function getSort()
    {
        return $this->sort;
    }

    /**
     * Set default_value
     *
     * @param string $defaultValue
     * @return FormField
     */
    public function setDefaultValue($defaultValue)
    {
        $this->default_value = $defaultValue;

        return $this;
    }

    /**
     * Get default_value
     *
     * @return string 
     */
    public function getDefaultValue()
    {
        return $this->default_value;
    }

    /**
     * Set is_active
     *
     * @param boolean $is_active
     * @return FormField
     */
    public function setIsActive($is_active)
    {
        $this->is_active = $is_active;

        return $this;
    }

    /**
     * Get is_active
     *
     * @return boolean 
     */
    public function getIsActive()
    {
        return $this->is_active;
    }
    
    public function isActive(){
        if($this->getIsActive())
            return true;
        return false;
    }

    /**
     * Set field
     *
     * @param \PiZone\FormBundle\Entity\Field $field
     * @return FormField
     */
    public function setField(\PiZone\FormBundle\Entity\Field $field = null)
    {
        $this->field = $field;

        return $this;
    }

    /**
     * Get field
     *
     * @return \PiZone\FormBundle\Entity\Field
     */
    public function getField()
    {
        return $this->field;
    }

    /**
     * Set alias
     *
     * @param string $alias
     * @return FormField
     */
    public function setAlias($alias)
    {
        $this->alias = $alias;

        return $this;
    }

    /**
     * Get alias
     *
     * @return string 
     */
    public function getAlias()
    {
        return $this->alias;
    }
    
    /**
     * Get form
     *
     * @return \PiZone\FormBundle\Entity\Form
     */
    public function getForm()
    {
        return $this->form;
    }

    /**
     * Set form
     *
     * @param \PiZone\FormBundle\Entity\Form $form
     * @return FormField
     */
    public function setForm(\PiZone\FormBundle\Entity\Form $form = null)
    {
        $this->form = $form;

        return $this;
    }

    /**
     * Set placeholder
     *
     * @param string $placeholder
     * @return FormField
     */
    public function setPlaceholder($placeholder)
    {
        $this->placeholder = $placeholder;

        return $this;
    }

    /**
     * Get placeholder
     *
     * @return string 
     */
    public function getPlaceholder()
    {
        return $this->placeholder;
    }

    /**
     * Set class
     *
     * @param string $class
     * @return FormField
     */
    public function setClass($class)
    {
        $this->class = $class;

        return $this;
    }

    /**
     * Get class
     *
     * @return string 
     */
    public function getClass()
    {
        return $this->class;
    }

    /**
     * Set attr
     *
     * @param string $attr
     * @return FormField
     */
    public function setAttr($attr)
    {
        $this->attr = $attr;

        return $this;
    }

    /**
     * Get attr
     *
     * @return string 
     */
    public function getAttr()
    {
        return $this->attr;
    }

    /**
     * Set is_required
     *
     * @param boolean $isRequired
     * @return FormField
     */
    public function setIsRequired($isRequired)
    {
        $this->is_required = $isRequired;

        return $this;
    }

    /**
     * Get is_required
     *
     * @return boolean 
     */
    public function getIsRequired()
    {
        return $this->is_required;
    }
    
    public function isRequired(){
        if($this->getIsRequired())
            return true;
        return false;
    }

    /**
     * Set regexp_pattern
     *
     * @param string $regexp_pattern
     * @return FormField
     */
    public function setRegexpPattern($regexp_pattern)
    {
        $this->regexp_pattern = $regexp_pattern;

        return $this;
    }

    /**
     * Get regexp_pattern
     *
     * @return string 
     */
    public function getRegexpPattern()
    {
        return $this->regexp_pattern;
    }
}
