<?php

namespace PiZone\FormBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Table(name="pz_status")
 * @ORM\Entity(repositoryClass="PiZone\FormBundle\Entity\Repository\StatusRepository")
 * @UniqueEntity(fields="alias", message="Sorry, this alias is already in use.", groups={"Status"})
 */
class Status {

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Please enter alias.", groups={"Status"})
     * @Assert\Regex( 
     *       pattern="/^[a-z,A-Z,\_,\-,0-9]+$/",
     *       message="Alias can contain only letters, numbers and symbols '_' , '-'.", 
     *       groups={"Status"}
     * )
     */
    protected $alias;
    
    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Please enter title.", groups={"Status"})
     */
    protected $title;
    
    /**
    * @ORM\Column(type="integer", nullable=true)
     */
    protected $sort = 100;
    
    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $is_active = true;
    
    /**
    * @ORM\OneToMany(targetEntity="FormStatus", mappedBy="status", cascade={"persist", "remove"})
    */
    protected $form_status;
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->form_status = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set alias
     *
     * @param string $alias
     * @return Status
     */
    public function setAlias($alias)
    {
        $this->alias = $alias;

        return $this;
    }

    /**
     * Get alias
     *
     * @return string 
     */
    public function getAlias()
    {
        return $this->alias;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Status
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set is_active
     *
     * @param boolean $isActive
     * @return Status
     */
    public function setIsActive($isActive)
    {
        $this->is_active = $isActive;

        return $this;
    }

    /**
     * Get is_active
     *
     * @return boolean 
     */
    public function getIsActive()
    {
        return $this->is_active;
    }

    /**
     * Add form_status
     *
     * @param \PiZone\FormBundle\Entity\FormStatus $formStatus
     * @return Status
     */
    public function addFormStatus(\PiZone\FormBundle\Entity\FormStatus $formStatus)
    {
        $this->form_status[] = $formStatus;

        return $this;
    }

    /**
     * Remove form_status
     *
     * @param \PiZone\FormBundle\Entity\FormStatus $formStatus
     */
    public function removeFormStatus(\PiZone\FormBundle\Entity\FormStatus $formStatus)
    {
        $this->form_status->removeElement($formStatus);
    }

    /**
     * Get form_status
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getFormStatus()
    {
        return $this->form_status;
    }

    /**
     * Set sort
     *
     * @param integer $sort
     * @return Status
     */
    public function setSort($sort)
    {
        $this->sort = $sort;

        return $this;
    }

    /**
     * Get sort
     *
     * @return integer 
     */
    public function getSort()
    {
        return $this->sort;
    }
}
