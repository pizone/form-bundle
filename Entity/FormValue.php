<?php

namespace PiZone\FormBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="pz_form_value")
 * @ORM\Entity(repositoryClass="PiZone\FormBundle\Entity\Repository\FormValueRepository")
 */
class FormValue {

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Form", inversedBy="values")
     * @ORM\JoinColumn(name="form_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $form;

//    /**
//     * @ORM\Column(type="string", length=255, nullable=true)
//     */
//    protected $username;
//
//    /**
//     * @ORM\Column(type="string", length=225, nullable=true)
//     * @Assert\Email(
//     *     message = "The email is not a valid email", groups={"Form"}
//     * )
//     */
//    protected $email;
//
//    /**
//     * @ORM\Column(type="string", length=255, nullable=true)
//     */
//    protected $phone;
//
//    /**
//     * @ORM\Column(type="text", nullable=true)
//     */
//    protected $text;

    /**
     * @var datetime $created
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    protected $created_at;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $is_active = true;

    /**
     * @ORM\OneToMany(targetEntity="FormStatus", mappedBy="form_value", cascade={"persist", "remove"})
     */
    protected $form_status;

    /**
     * @ORM\OneToMany(targetEntity="FormComment", mappedBy="form_value", cascade={"persist", "remove"})
     */
    protected $form_comment;
    protected $more;
    protected $path;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $more_field;

    /**
     * Constructor
     */
    public function __construct($object = null) {
        if ($object) {
            if (isset($object['more_fields']))
                $this->setMoreField($object['more_fields']);
        }
        $this->form_status = new \Doctrine\Common\Collections\ArrayCollection();
        $this->form_comment = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

//    /**
//     * Set username
//     *
//     * @param string $username
//     * @return FormValue
//     */
//    public function setUsername($username) {
//        $this->username = $username;
//
//        return $this;
//    }
//
//    /**
//     * Get username
//     *
//     * @return string
//     */
//    public function getUsername() {
//        return $this->username;
//    }
//
//    /**
//     * Set email
//     *
//     * @param string $email
//     * @return FormValue
//     */
//    public function setEmail($email) {
//        $this->email = $email;
//
//        return $this;
//    }
//
//    /**
//     * Get email
//     *
//     * @return string
//     */
//    public function getEmail() {
//        return $this->email;
//    }
//
//    /**
//     * Set phone
//     *
//     * @param string $phone
//     * @return FormValue
//     */
//    public function setPhone($phone) {
//        $this->phone = $phone;
//
//        return $this;
//    }
//
//    /**
//     * Get phone
//     *
//     * @return string
//     */
//    public function getPhone() {
//        return $this->phone;
//    }
//
//    /**
//     * Set text
//     *
//     * @param string $text
//     * @return FormValue
//     */
//    public function setText($text) {
//        $this->text = $text;
//
//        return $this;
//    }
//
//    /**
//     * Get text
//     *
//     * @return string
//     */
//    public function getText() {
//        return $this->text;
//    }

    /**
     * Set created_at
     *
     * @param \DateTime $createdAt
     * @return FormValue
     */
    public function setCreatedAt($createdAt) {
        $this->created_at = $createdAt;

        return $this;
    }

    /**
     * Get created_at
     *
     * @return \DateTime 
     */
    public function getCreatedAt() {
        return $this->created_at;
    }

    /**
     * Set is_active
     *
     * @param boolean $isActive
     * @return FormValue
     */
    public function setIsActive($isActive) {
        $this->is_active = $isActive;

        return $this;
    }

    /**
     * Get is_active
     *
     * @return boolean 
     */
    public function getIsActive() {
        return $this->is_active;
    }

    /**
     * Set form
     *
     * @param \PiZone\FormBundle\Entity\Form $form
     * @return FormValue
     */
    public function setForm(\PiZone\FormBundle\Entity\Form $form = null) {
        $this->form = $form;

        return $this;
    }

    /**
     * Get form
     *
     * @return \PiZone\FormBundle\Entity\Form
     */
    public function getForm() {
        return $this->form;
    }

    /**
     * Add form_status
     *
     * @param \PiZone\FormBundle\Entity\FormStatus $formStatus
     * @return FormValue
     */
    public function addFormStatus(\PiZone\FormBundle\Entity\FormStatus $formStatus) {
        $this->form_status[] = $formStatus;
        $formStatus->setFormValue($this);

        return $this;
    }

    /**
     * Remove form_status
     *
     * @param \PiZone\FormBundle\Entity\FormStatus $formStatus
     */
    public function removeFormStatus(\PiZone\FormBundle\Entity\FormStatus $formStatus) {
        $this->form_status->removeElement($formStatus);
    }

    /**
     * Get form_status
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getFormStatus() {
        return $this->form_status;
    }

    /**
     * Add form_comment
     *
     * @param \PiZone\FormBundle\Entity\FormComment $formComment
     * @return FormValue
     */
    public function addFormComment(\PiZone\FormBundle\Entity\FormComment $formComment) {
        $this->form_comment[] = $formComment;
        $formComment->setFormValue($this);

        return $this;
    }

    /**
     * Remove form_comment
     *
     * @param \PiZone\FormBundle\Entity\FormComment $formComment
     */
    public function removeFormComment(\PiZone\FormBundle\Entity\FormComment $formComment) {
        $this->form_comment->removeElement($formComment);
    }

    /**
     * Get form_comment
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getFormComment() {
        return $this->form_comment;
    }

    /**
     * Set more_field
     *
     * @param string $more_field
     * @return FormValue
     */
    public function setMoreField($more_field) {
        $this->more_field = json_encode($more_field);

        return $this;
    }

    /**
     * Get more_field
     *
     * @return string 
     */
    public function getMoreField() {
        if ($this->more_field)
            return json_decode($this->more_field, true);
        return array();
    }

    public function getMore() {
        return $this->more;
    }

    public function setMore($more_field) {
        $this->setMoreField($more_field);
    }

    public function getIdLastStatus() {
        $last = $this->getFormStatus()->last();
        if (!$last) {
            return 0;
        }
        return $this->getFormStatus()->last()->getStatus()->getId();
    }

    public function renderFieldUsername($value = null) {
        if (!$value)
            $value = htmlspecialchars($this->getUsername());
        $str = '<input type="text" name="' . $this->form->getAlias() . '[username]" value="' . $value . '" id="' . $this->form->getAlias() . '_text_username"';
        if ($this->form->getUsernamePlaceholder())
            $str .= ' placeholder="' . $this->form->getUsernamePlaceholder() . '"';
        if ($this->form->getUsernameAttr())
            $str .= ' ' . $this->form->getUsernameAttr();
        if ($this->form->getUsernameClass())
            $str .= ' class="' . $this->form->getUsernameClass() . '"';
        $str .= '/>';
        return $str;
    }

    public function renderFieldEmail($value = null) {
        if (!$value)
            $value = htmlspecialchars($this->getEmail());
        $str = '<input type="text" name="' . $this->form->getAlias() . '[email]" value="' . $value . '" id="' . $this->form->getAlias() . '_text_email"';
        if ($this->form->getEmailPlaceholder())
            $str .= ' placeholder="' . $this->form->getEmailPlaceholder() . '"';
        if ($this->form->getEmailAttr())
            $str .= ' ' . $this->form->getEmailAttr();
        if ($this->form->getEmailClass())
            $str .= ' class="' . $this->form->getEmailClass() . '"';
        $str .= '/>';
        return $str;
    }

    public function renderFieldPhone($value = null) {
        if (!$value)
            $value = htmlspecialchars($this->getPhone());
        $str = '<input type="text" name="' . $this->form->getAlias() . '[phone]" value="' . $value . '" id="' . $this->form->getAlias() . '_text_phone"';
        if ($this->form->getPhonePlaceholder())
            $str .= ' placeholder="' . $this->form->getPhonePlaceholder() . '"';
        if ($this->form->getPhoneAttr())
            $str .= ' ' . $this->form->getPhoneAttr();
        if ($this->form->getPhoneClass())
            $str .= ' class="' . $this->form->getPhoneClass() . '"';
        $str .= '/>';
        return $str;
    }

    public function renderFieldText($value = null) {
        if (!$value)
            $value = htmlspecialchars($this->getText());
        $str = '<textarea name="' . $this->form->getAlias() . '[text]" id="' . $this->form->getAlias() . '_textarea_text"';
        if ($this->form->getTextPlaceholder())
            $str .= ' placeholder="' . $this->form->getTextPlaceholder() . '"';
        if ($this->form->getTextAttr())
            $str .= ' ' . $this->form->getTextAttr();
        if ($this->form->getTextClass())
            $str .= ' class="' . $this->form->getTextClass() . '"';
        $str .= '>' . $value . '</textarea>';
        return $str;
    }

    public function getLabelForUsername($form) {
        return '<label for="' . $form->getAlias() . '_username">' . $form->getUsernameLabel() . '</label>';
    }

    public function getLabelForEmail($form) {
        return '<label for="' . $form->getAlias() . '_email">' . $form->getEmailLabel() . '</label>';
    }

    public function getLabelForPhone($form) {
        return '<label for="' . $form->getAlias() . '_phone">' . $form->getPhoneLabel() . '</label>';
    }

    public function getlabelForText($form) {
        return '<label for="' . $form->getAlias() . '_text">' . $form->getTextLabel() . '</label>';
    }

    public function addFiles($files) {
        foreach ($files as $one) {
            $this->addFile($one);
        }
    }

    public function addFile($file) {
        if ($file['file']) {
            $this->upload($file);
        }
    }

    public function getPath() {
        return $this->path;
    }

    public function getOriginName() {
        return $this->file_origin_name;
    }

    public function getTemp() {
        return $this->temp;
    }

    public function getAbsolutePath($path) {
        return null === $path ? null : $this->getUploadRootDir() . '/' . $path;
    }

    public function getWebPath($path) {
        return null === $path ? null : '/' . $this->getUploadDir() . '/' . $path;
    }

    protected function getUploadRootDir() {
        return __DIR__ . '/../../../../web/' . $this->getUploadDir();
    }

    protected function getUploadDir() {
        return 'uploads/files';
    }

    public function upload($file) {
        $filename = sha1(uniqid(mt_rand(), true));
        $this->path = $filename . '.' . $file['extention'];
        $fileData = array($file['key'] => array('path' => $this->path, 'origin_name' => $file['origin_name']));

        $file['file']->move($this->getUploadRootDir(), $this->path);

        $data = $this->getMoreField();
        $data = $data + $fileData;

        $this->setMoreField($data);
    }

}
