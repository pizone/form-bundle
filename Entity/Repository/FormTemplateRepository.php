<?php

namespace PiZone\FormBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * FormTemplateRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class FormTemplateRepository extends EntityRepository
{
    public function GetFormTemplates($formId){
        $q = $this->_em->createQueryBuilder();
        $query = $q->select("t")
                ->from("PiZoneFormBundle:FormTemplate", "t")
                ->innerJoin('t.parent_form', 'f')
                ->where('f.id = :formId')
                ->setParameter('formId', $formId);
        return $query;
    }
}
