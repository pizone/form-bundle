<?php

namespace PiZone\FormBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Table(name="pz_event")
 * @ORM\Entity(repositoryClass="PiZone\FormBundle\Entity\Repository\EventRepository")
 * @UniqueEntity(fields="alias", message="Sorry, this alias is already in use.", groups={"Event"})
 */
class Event {

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Please enter alias.", groups={"Event"})
     * @Assert\Regex( 
     *       pattern="/^[a-z,A-Z,\_,\-,0-9]+$/",
     *       message="Alias can contain only letters, numbers and symbols '_' , '-'.", 
     *       groups={"Event"}
     * )
     */
    protected $alias;
    
    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Please enter title.", groups={"Event"})
     */
    protected $title;
    
    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $description;
    
    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Please enter subject.", groups={"Event"})
     */
    protected $subject;
    
    /**
     * @ORM\Column(type="text")
     * @Assert\NotBlank(message="Please enter content.", groups={"Event"})
     */
    protected $content;
    
    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $is_active = true;
    
    /**
     * @ORM\ManyToMany(targetEntity="Form", mappedBy="events")
     */
    protected $forms;
    
    /**
    * @ORM\OneToMany(targetEntity="FormEvent", mappedBy="event", cascade={"persist", "remove"})
    */
    protected $form_event;
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->forms = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set alias
     *
     * @param string $alias
     * @return Event
     */
    public function setAlias($alias)
    {
        $this->alias = $alias;

        return $this;
    }

    /**
     * Get alias
     *
     * @return string 
     */
    public function getAlias()
    {
        return $this->alias;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Event
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Event
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set content
     *
     * @param string $content
     * @return Event
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string 
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set is_active
     *
     * @param boolean $isActive
     * @return Event
     */
    public function setIsActive($isActive)
    {
        $this->is_active = $isActive;

        return $this;
    }

    /**
     * Get is_active
     *
     * @return boolean 
     */
    public function getIsActive()
    {
        return $this->is_active;
    }

    /**
     * Add forms
     *
     * @param \PiZone\FormBundle\Entity\Form $forms
     * @return Event
     */
    public function addForm(\PiZone\FormBundle\Entity\Form $forms)
    {
        $this->forms[] = $forms;

        return $this;
    }

    /**
     * Remove forms
     *
     * @param \PiZone\FormBundle\Entity\Form $forms
     */
    public function removeForm(\PiZone\FormBundle\Entity\Form $forms)
    {
        $this->forms->removeElement($forms);
    }

    /**
     * Get forms
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getForms()
    {
        return $this->forms;
    }
    
    public function __toString() {
        return $this->title;
    }

    /**
     * Set subject
     *
     * @param string $subject
     * @return Event
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;

        return $this;
    }

    /**
     * Get subject
     *
     * @return string 
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * Add form_event
     *
     * @param \PiZone\FormBundle\Entity\FormEvent $formEvent
     * @return Event
     */
    public function addFormEvent(\PiZone\FormBundle\Entity\FormEvent $formEvent)
    {
        $this->form_event[] = $formEvent;

        return $this;
    }

    /**
     * Remove form_event
     *
     * @param \PiZone\FormBundle\Entity\FormEvent $formEvent
     */
    public function removeFormEvent(\PiZone\FormBundle\Entity\FormEvent $formEvent)
    {
        $this->form_event->removeElement($formEvent);
    }

    /**
     * Get form_event
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getFormEvent()
    {
        return $this->form_event;
    }
}
