<?php

namespace PiZone\FormBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="pz_form_comment")
 * @ORM\Entity(repositoryClass="PiZone\FormBundle\Entity\Repository\FormCommentRepository")
 */
class FormComment {

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * @ORM\ManyToOne(targetEntity="FormValue", inversedBy="form_comment")
     * @ORM\JoinColumn(name="form_value_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $form_value;
    
    /**
     * @ORM\ManyToOne(targetEntity="PiZone\UserBundle\Entity\User", inversedBy="form_comment")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $user;
    
    /**
     * @ORM\Column(type="text")
     * @Assert\NotBlank(message="Required field", groups={"Comment"})
     */
    protected $comment;
    
    /**
     * @var datetime $created
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    protected $created_at;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set comment
     *
     * @param string $comment
     * @return FormComment
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string 
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set created_at
     *
     * @param \DateTime $createdAt
     * @return FormComment
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;

        return $this;
    }

    /**
     * Get created_at
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set form_value
     *
     * @param \PiZone\FormBundle\Entity\FormValue $formValue
     * @return FormComment
     */
    public function setFormValue(\PiZone\FormBundle\Entity\FormValue $formValue = null)
    {
        $this->form_value = $formValue;

        return $this;
    }

    /**
     * Get form_value
     *
     * @return \PiZone\FormBundle\Entity\FormValue 
     */
    public function getFormValue()
    {
        return $this->form_value;
    }

    /**
     * Set user
     *
     * @param \PiZone\UserBundle\Entity\User $user
     * @return FormComment
     */
    public function setUser(\PiZone\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \PiZone\UserBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }
}
