<?php

namespace PiZone\FormBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Table(name="pz_form")
 * @ORM\Entity(repositoryClass="PiZone\FormBundle\Entity\Repository\FormRepository")
 * @UniqueEntity(fields="alias", message="Sorry, this alias is already in use.", groups={"Form"})
 */
class Form {

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * @ORM\ManyToMany(targetEntity="PiZone\UserBundle\Entity\User", mappedBy="forms")
     */
    protected $users;
    
    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Please enter alias.", groups={"Form"})
     * @Assert\Regex( 
     *       pattern="/^[a-z,A-Z,\_,\-,0-9]+$/",
     *       message="Alias can contain only letters, numbers and symbols '_' , '-'.", 
     *       groups={"Form"}
     * )
     */
    protected $alias;
    
    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Please enter title.", groups={"Form"})
     */
    protected $title;
    
    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $description;
    
    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $is_active = true;
    
    /**
    * @ORM\OneToMany(targetEntity="FormValue", mappedBy="form", cascade={"persist", "remove"})
    */
    protected $values;

    /**
     * @ORM\ManyToOne(targetEntity="FormTemplate", inversedBy="form")
     * @ORM\JoinColumn(name="form_template_id", referencedColumnName="id", onDelete="SET NULL")
     */
    protected $template;
    
    /**
    * @ORM\OneToMany(targetEntity="FormTemplate", mappedBy="parent_form", cascade={"persist", "remove"})
    */
    protected $form_templates;
    
    /**
     * @ORM\ManyToMany(targetEntity="Event", inversedBy="forms")
     * @ORM\JoinTable(name="pz_form_form_event",
     *      joinColumns={@ORM\JoinColumn(name="form_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="event_id", referencedColumnName="id")}
     * )
     */
    protected $events;
    
    /**
    * @ORM\OneToMany(targetEntity="FormEvent", mappedBy="form", cascade={"persist", "remove"})
    */
    protected $form_event;

    /**
    * @ORM\OneToMany(targetEntity="FormField", mappedBy="form", cascade={"persist", "remove"})
    */
    protected $form_fields;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $mail_to;
    
    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $domains;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->values = new \Doctrine\Common\Collections\ArrayCollection();
        $this->events = new \Doctrine\Common\Collections\ArrayCollection();
        $this->form_event = new \Doctrine\Common\Collections\ArrayCollection();
        $this->form_fields = new \Doctrine\Common\Collections\ArrayCollection();
        $this->form_templates = new \Doctrine\Common\Collections\ArrayCollection();
        $this->users = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    public function __toString() {
        return $this->getTitle();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set alias
     *
     * @param string $alias
     * @return Form
     */
    public function setAlias($alias)
    {
        $this->alias = $alias;

        return $this;
    }

    /**
     * Get alias
     *
     * @return string 
     */
    public function getAlias()
    {
        return $this->alias;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Form
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Form
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set is_active
     *
     * @param boolean $isActive
     * @return Form
     */
    public function setIsActive($isActive)
    {
        $this->is_active = $isActive;

        return $this;
    }

    /**
     * Get is_active
     *
     * @return boolean 
     */
    public function getIsActive()
    {
        return $this->is_active;
    }

    /**
     * Add values
     *
     * @param \PiZone\FormBundle\Entity\FormValue $values
     * @return Form
     */
    public function addValue(\PiZone\FormBundle\Entity\FormValue $values)
    {
        $this->values[] = $values;

        return $this;
    }

    /**
     * Remove values
     *
     * @param \PiZone\FormBundle\Entity\FormValue $values
     */
    public function removeValue(\PiZone\FormBundle\Entity\FormValue $values)
    {
        $this->values->removeElement($values);
    }

    /**
     * Get values
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getValues()
    {
        return $this->values;
    }

    /**
     * Add events
     *
     * @param \PiZone\FormBundle\Entity\Event $events
     * @return Form
     */
    public function addEvent(\PiZone\FormBundle\Entity\Event $events)
    {
        $this->events[] = $events;

        return $this;
    }

    /**
     * Remove events
     *
     * @param \PiZone\FormBundle\Entity\Event $events
     */
    public function removeEvent(\PiZone\FormBundle\Entity\Event $events)
    {
        $this->events->removeElement($events);
    }

    /**
     * Get events
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getEvents()
    {
        return $this->events;
    }

    /**
     * Get form_fields
     *
     * @return \PiZone\FormBundle\Entity\FormField 
     */
    public function getFormFields()
    {
        return $this->form_fields;
    }

    /**
     * Add form_fields
     *
     * @param \PiZone\FormBundle\Entity\FormField $formFields
     * @return Form
     */
    public function addFormField(\PiZone\FormBundle\Entity\FormField $formFields)
    {
        $this->form_fields[] = $formFields;
        $formFields->setForm($this);
        return $this;
    }

    /**
     * Remove form_fields
     *
     * @param \PiZone\FormBundle\Entity\FormField $formFields
     */
    public function removeFormField(\PiZone\FormBundle\Entity\FormField $formFields)
    {
        $this->form_fields->removeElement($formFields);
    }

    /**
     * Set template
     *
     * @param \PiZone\FormBundle\Entity\FormTemplate $template
     * @return Form
     */
    public function setTemplate(\PiZone\FormBundle\Entity\FormTemplate $template = null)
    {
        $this->template = $template;

        return $this;
    }
    
    
    /**
     * Get template
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTemplate()
    {
        return $this->template;
    }

//    /**
//     * Set username_label
//     *
//     * @param string $usernameLabel
//     * @return Form
//     */
//    public function setUsernameLabel($usernameLabel)
//    {
//        $this->username_label = $usernameLabel;
//
//        return $this;
//    }
//
//    /**
//     * Get username_label
//     *
//     * @return string
//     */
//    public function getUsernameLabel()
//    {
//        return $this->username_label;
//    }
//
//    /**
//     * Set email_label
//     *
//     * @param string $emailLabel
//     * @return Form
//     */
//    public function setEmailLabel($emailLabel)
//    {
//        $this->email_label = $emailLabel;
//
//        return $this;
//    }
//
//    /**
//     * Get email_label
//     *
//     * @return string
//     */
//    public function getEmailLabel()
//    {
//        return $this->email_label;
//    }
//
//    /**
//     * Set phone_label
//     *
//     * @param string $phoneLabel
//     * @return Form
//     */
//    public function setPhoneLabel($phoneLabel)
//    {
//        $this->phone_label = $phoneLabel;
//
//        return $this;
//    }
//
//    /**
//     * Get phone_label
//     *
//     * @return string
//     */
//    public function getPhoneLabel()
//    {
//        return $this->phone_label;
//    }
//
//    /**
//     * Set text_label
//     *
//     * @param string $textLabel
//     * @return Form
//     */
//    public function setTextLabel($textLabel)
//    {
//        $this->text_label = $textLabel;
//
//        return $this;
//    }
//
//    /**
//     * Get text_label
//     *
//     * @return string
//     */
//    public function getTextLabel()
//    {
//        return $this->text_label;
//    }

    /**
     * Add users
     *
     * @param \PiZone\UserBundle\Entity\User $users
     * @return Form
     */
    public function addUser(\PiZone\UserBundle\Entity\User $users)
    {
        $this->users[] = $users;

        return $this;
    }

    /**
     * Remove users
     *
     * @param \PiZone\UserBundle\Entity\User $users
     */
    public function removeUser(\PiZone\UserBundle\Entity\User $users)
    {
        $this->users->removeElement($users);
    }

    /**
     * Get users
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * Set mail_to
     *
     * @param string $mailTo
     * @return Form
     */
    public function setMailTo($mailTo)
    {
        $this->mail_to = $mailTo;

        return $this;
    }

    /**
     * Get mail_to
     *
     * @return string 
     */
    public function getMailTo()
    {
        return $this->mail_to;
    }

    /**
     * Add form_event
     *
     * @param \PiZone\FormBundle\Entity\FormEvent $formEvent
     * @return Form
     */
    public function addFormEvent(\PiZone\FormBundle\Entity\FormEvent $formEvent)
    {
        $this->form_event[] = $formEvent;
        $formEvent->setForm($this);
        return $this;
    }

    /**
     * Remove form_event
     *
     * @param \PiZone\FormBundle\Entity\FormEvent $formEvent
     */
    public function removeFormEvent(\PiZone\FormBundle\Entity\FormEvent $formEvent)
    {
        $this->form_event->removeElement($formEvent);
    }

    /**
     * Get form_event
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getFormEvent()
    {
        return $this->form_event;
    }

    /**
     * Add form_templates
     *
     * @param \PiZone\FormBundle\Entity\FormTemplate $formTemplates
     * @return Form
     */
    public function addFormTemplate(\PiZone\FormBundle\Entity\FormTemplate $formTemplates)
    {
        $this->form_templates[] = $formTemplates;
        $formTemplates->setParentForm($this);

        return $this;
    }

    /**
     * Remove form_templates
     *
     * @param \PiZone\FormBundle\Entity\FormTemplate $formTemplates
     */
    public function removeFormTemplate(\PiZone\FormBundle\Entity\FormTemplate $formTemplates)
    {
        $this->form_templates->removeElement($formTemplates);
    }

    /**
     * Get form_templates
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getFormTemplates()
    {
        return $this->form_templates;
    }

    /**
     * Set domains
     *
     * @param string $domains
     * @return Form
     */
    public function setDomains($domains)
    {
        $this->domains = $domains;

        return $this;
    }

    /**
     * Get domains
     *
     * @return string 
     */
    public function getDomains()
    {
        return $this->domains;
    }
//
//    /**
//     * Set username_placeholder
//     *
//     * @param string $usernamePlaceholder
//     * @return Form
//     */
//    public function setUsernamePlaceholder($usernamePlaceholder)
//    {
//        $this->username_placeholder = $usernamePlaceholder;
//
//        return $this;
//    }
//
//    /**
//     * Get username_placeholder
//     *
//     * @return string
//     */
//    public function getUsernamePlaceholder()
//    {
//        return $this->username_placeholder;
//    }
//
//    /**
//     * Set email_placeholder
//     *
//     * @param string $emailPlaceholder
//     * @return Form
//     */
//    public function setEmailPlaceholder($emailPlaceholder)
//    {
//        $this->email_placeholder = $emailPlaceholder;
//
//        return $this;
//    }
//
//    /**
//     * Get email_placeholder
//     *
//     * @return string
//     */
//    public function getEmailPlaceholder()
//    {
//        return $this->email_placeholder;
//    }
//
//    /**
//     * Set phone_placeholder
//     *
//     * @param string $phonePlaceholder
//     * @return Form
//     */
//    public function setPhonePlaceholder($phonePlaceholder)
//    {
//        $this->phone_placeholder = $phonePlaceholder;
//
//        return $this;
//    }
//
//    /**
//     * Get phone_placeholder
//     *
//     * @return string
//     */
//    public function getPhonePlaceholder()
//    {
//        return $this->phone_placeholder;
//    }
//
//    /**
//     * Set text_placeholder
//     *
//     * @param string $textPlaceholder
//     * @return Form
//     */
//    public function setTextPlaceholder($textPlaceholder)
//    {
//        $this->text_placeholder = $textPlaceholder;
//
//        return $this;
//    }
//
//    /**
//     * Get text_placeholder
//     *
//     * @return string
//     */
//    public function getTextPlaceholder()
//    {
//        return $this->text_placeholder;
//    }
//
//    /**
//     * Set username_class
//     *
//     * @param string $usernameClass
//     * @return Form
//     */
//    public function setUsernameClass($usernameClass)
//    {
//        $this->username_class = $usernameClass;
//
//        return $this;
//    }
//
//    /**
//     * Get username_class
//     *
//     * @return string
//     */
//    public function getUsernameClass()
//    {
//        return $this->username_class;
//    }
//
//    /**
//     * Set email_class
//     *
//     * @param string $emailClass
//     * @return Form
//     */
//    public function setEmailClass($emailClass)
//    {
//        $this->email_class = $emailClass;
//
//        return $this;
//    }
//
//    /**
//     * Get email_class
//     *
//     * @return string
//     */
//    public function getEmailClass()
//    {
//        return $this->email_class;
//    }
//
//    /**
//     * Set phone_class
//     *
//     * @param string $phoneClass
//     * @return Form
//     */
//    public function setPhoneClass($phoneClass)
//    {
//        $this->phone_class = $phoneClass;
//
//        return $this;
//    }
//
//    /**
//     * Get phone_class
//     *
//     * @return string
//     */
//    public function getPhoneClass()
//    {
//        return $this->phone_class;
//    }
//
//    /**
//     * Set text_class
//     *
//     * @param string $textClass
//     * @return Form
//     */
//    public function setTextClass($textClass)
//    {
//        $this->text_class = $textClass;
//
//        return $this;
//    }
//
//    /**
//     * Get text_class
//     *
//     * @return string
//     */
//    public function getTextClass()
//    {
//        return $this->text_class;
//    }
//
//    /**
//     * Set username_attr
//     *
//     * @param string $usernameAttr
//     * @return Form
//     */
//    public function setUsernameAttr($usernameAttr)
//    {
//        $this->username_attr = $usernameAttr;
//
//        return $this;
//    }
//
//    /**
//     * Get username_attr
//     *
//     * @return string
//     */
//    public function getUsernameAttr()
//    {
//        return $this->username_attr;
//    }
//
//    /**
//     * Set username_is_required
//     *
//     * @param boolean $usernameIsRequired
//     * @return Form
//     */
//    public function setUsernameIsRequired($usernameIsRequired)
//    {
//        $this->username_is_required = $usernameIsRequired;
//
//        return $this;
//    }
//
//    /**
//     * Get username_is_required
//     *
//     * @return boolean
//     */
//    public function getUsernameIsRequired()
//    {
//        return $this->username_is_required;
//    }
//
//    /**
//     * Set username_sort
//     *
//     * @param integer $usernameSort
//     * @return Form
//     */
//    public function setUsernameSort($usernameSort)
//    {
//        $this->username_sort = $usernameSort;
//
//        return $this;
//    }
//
//    /**
//     * Get username_sort
//     *
//     * @return integer
//     */
//    public function getUsernameSort()
//    {
//        return $this->username_sort;
//    }
//
//    /**
//     * Set username_is_active
//     *
//     * @param boolean $usernameIsActive
//     * @return Form
//     */
//    public function setUsernameIsActive($usernameIsActive)
//    {
//        $this->username_is_active = $usernameIsActive;
//
//        return $this;
//    }
//
//    /**
//     * Get username_is_active
//     *
//     * @return boolean
//     */
//    public function getUsernameIsActive()
//    {
//        return $this->username_is_active;
//    }
//
//    /**
//     * Set email_is_required
//     *
//     * @param boolean $emailIsRequired
//     * @return Form
//     */
//    public function setEmailIsRequired($emailIsRequired)
//    {
//        $this->email_is_required = $emailIsRequired;
//
//        return $this;
//    }
//
//    /**
//     * Get email_is_required
//     *
//     * @return boolean
//     */
//    public function getEmailIsRequired()
//    {
//        return $this->email_is_required;
//    }
//
//    /**
//     * Set email_attr
//     *
//     * @param string $emailAttr
//     * @return Form
//     */
//    public function setEmailAttr($emailAttr)
//    {
//        $this->email_attr = $emailAttr;
//
//        return $this;
//    }
//
//    /**
//     * Get email_attr
//     *
//     * @return string
//     */
//    public function getEmailAttr()
//    {
//        return $this->email_attr;
//    }
//
//    /**
//     * Set email_sort
//     *
//     * @param integer $emailSort
//     * @return Form
//     */
//    public function setEmailSort($emailSort)
//    {
//        $this->email_sort = $emailSort;
//
//        return $this;
//    }
//
//    /**
//     * Get email_sort
//     *
//     * @return integer
//     */
//    public function getEmailSort()
//    {
//        return $this->email_sort;
//    }
//
//    /**
//     * Set email_is_active
//     *
//     * @param boolean $emailIsActive
//     * @return Form
//     */
//    public function setEmailIsActive($emailIsActive)
//    {
//        $this->email_is_active = $emailIsActive;
//
//        return $this;
//    }
//
//    /**
//     * Get email_is_active
//     *
//     * @return boolean
//     */
//    public function getEmailIsActive()
//    {
//        return $this->email_is_active;
//    }
//
//    /**
//     * Set phone_attr
//     *
//     * @param string $phoneAttr
//     * @return Form
//     */
//    public function setPhoneAttr($phoneAttr)
//    {
//        $this->phone_attr = $phoneAttr;
//
//        return $this;
//    }
//
//    /**
//     * Get phone_attr
//     *
//     * @return string
//     */
//    public function getPhoneAttr()
//    {
//        return $this->phone_attr;
//    }
//
//    /**
//     * Set phone_is_required
//     *
//     * @param boolean $phoneIsRequired
//     * @return Form
//     */
//    public function setPhoneIsRequired($phoneIsRequired)
//    {
//        $this->phone_is_required = $phoneIsRequired;
//
//        return $this;
//    }
//
//    /**
//     * Get phone_is_required
//     *
//     * @return boolean
//     */
//    public function getPhoneIsRequired()
//    {
//        return $this->phone_is_required;
//    }
//
//    /**
//     * Set phone_sort
//     *
//     * @param integer $phoneSort
//     * @return Form
//     */
//    public function setPhoneSort($phoneSort)
//    {
//        $this->phone_sort = $phoneSort;
//
//        return $this;
//    }
//
//    /**
//     * Get phone_sort
//     *
//     * @return integer
//     */
//    public function getPhoneSort()
//    {
//        return $this->phone_sort;
//    }
//
//    /**
//     * Set phone_is_active
//     *
//     * @param boolean $phoneIsActive
//     * @return Form
//     */
//    public function setPhoneIsActive($phoneIsActive)
//    {
//        $this->phone_is_active = $phoneIsActive;
//
//        return $this;
//    }
//
//    /**
//     * Get phone_is_active
//     *
//     * @return boolean
//     */
//    public function getPhoneIsActive()
//    {
//        return $this->phone_is_active;
//    }
//
//    /**
//     * Set text_attr
//     *
//     * @param string $textAttr
//     * @return Form
//     */
//    public function setTextAttr($textAttr)
//    {
//        $this->text_attr = $textAttr;
//
//        return $this;
//    }
//
//    /**
//     * Get text_attr
//     *
//     * @return string
//     */
//    public function getTextAttr()
//    {
//        return $this->text_attr;
//    }
//
//    /**
//     * Set text_is_required
//     *
//     * @param boolean $textIsRequired
//     * @return Form
//     */
//    public function setTextIsRequired($textIsRequired)
//    {
//        $this->text_is_required = $textIsRequired;
//
//        return $this;
//    }
//
//    /**
//     * Get text_is_required
//     *
//     * @return boolean
//     */
//    public function getTextIsRequired()
//    {
//        return $this->text_is_required;
//    }
//
//    /**
//     * Set text_sort
//     *
//     * @param integer $textSort
//     * @return Form
//     */
//    public function setTextSort($textSort)
//    {
//        $this->text_sort = $textSort;
//
//        return $this;
//    }
//
//    /**
//     * Get text_sort
//     *
//     * @return integer
//     */
//    public function getTextSort()
//    {
//        return $this->text_sort;
//    }
//
//    /**
//     * Set text_is_active
//     *
//     * @param boolean $textIsActive
//     * @return Form
//     */
//    public function setTextIsActive($textIsActive)
//    {
//        $this->text_is_active = $textIsActive;
//
//        return $this;
//    }
//
//    /**
//     * Get text_is_active
//     *
//     * @return boolean
//     */
//    public function getTextIsActive()
//    {
//        return $this->text_is_active;
//    }
//
//    public function UsernameIsActive(){
//        if($this->getUsernameIsActive())
//            return true;
//        return false;
//    }
//
//    public function EmailIsActive(){
//        if($this->getEmailIsActive())
//            return true;
//        return false;
//    }
//
//    public function PhoneIsActive(){
//        if($this->getPhoneIsActive())
//            return true;
//        return false;
//    }
//
//    public function TextIsActive(){
//        if($this->getTextIsActive())
//            return true;
//        return false;
//    }
}
