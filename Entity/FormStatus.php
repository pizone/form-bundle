<?php

namespace PiZone\FormBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use PiZone\UserBundle\Entity\User;

/**
 * @ORM\Table(name="pz_form_status")
 * @ORM\Entity(repositoryClass="PiZone\FormBundle\Entity\Repository\FormStatusRepository")
 */
class FormStatus {

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * @ORM\ManyToOne(targetEntity="FormValue", inversedBy="form_status")
     * @ORM\JoinColumn(name="form_value_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $form_value;
    
    /**
     * @ORM\ManyToOne(targetEntity="Status", inversedBy="form_status")
     * @ORM\JoinColumn(name="status_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $status;
    
    /**
     * @ORM\ManyToOne(targetEntity="PiZone\UserBundle\Entity\User", inversedBy="form_status")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $user;

    /**
     * @var datetime $created
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    protected $created_at;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set created_at
     *
     * @param \DateTime $createdAt
     * @return FormStatus
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;

        return $this;
    }

    /**
     * Get created_at
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set status
     *
     * @param \PiZone\FormBundle\Entity\Status $status
     * @return FormStatus
     */
    public function setStatus(\PiZone\FormBundle\Entity\Status $status = null)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return \PiZone\FormBundle\Entity\Status
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set form_value
     *
     * @param \PiZone\FormBundle\Entity\FormValue $formValue
     * @return FormStatus
     */
    public function setFormValue(\PiZone\FormBundle\Entity\FormValue $formValue = null)
    {
        $this->form_value = $formValue;

        return $this;
    }

    /**
     * Get form_value
     *
     * @return \PiZone\FormBundle\Entity\FormValue
     */
    public function getFormValue()
    {
        return $this->form_value;
    }

    /**
     * Set user
     *
     * @param \PiZone\UserBundle\Entity\User $user
     * @return FormStatus
     */
    public function setUser(\PiZone\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \PiZone\UserBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }
}
