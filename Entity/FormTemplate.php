<?php

namespace PiZone\FormBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="pz_form_template")
 * @ORM\Entity(repositoryClass="PiZone\FormBundle\Entity\Repository\FormTemplateRepository")
 */
class FormTemplate {

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Please enter title.", groups={"Form"})
     */
    protected $title;
    
    /**
    * @ORM\OneToMany(targetEntity="Form", mappedBy="template")
    */
    protected $form;
    
    /**
     * @ORM\Column(type="text")
     * @Assert\NotBlank(message="Required field", groups={"Form"})
     */
    protected $template;
    
    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $css;
    
    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $js;
    
    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $is_active = true;
    
    /**
     * @ORM\Column(type="string", length=1000, nullable=true)
     */
    protected $url;
    
    /**
     * @ORM\ManyToOne(targetEntity="Form", inversedBy="form_templates")
     * @ORM\JoinColumn(name="form_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $parent_form;
    
    protected $is_selected;
            

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set template
     *
     * @param string $template
     * @return FormTemplate
     */
    public function setTemplate($template)
    {
        $this->template = $template;

        return $this;
    }

    /**
     * Get template
     *
     * @return string 
     */
    public function getTemplate()
    {
        return $this->template;
    }

    /**
     * Set css
     *
     * @param string $css
     * @return FormTemplate
     */
    public function setCss($css)
    {
        $this->css = $css;

        return $this;
    }

    /**
     * Get css
     *
     * @return string 
     */
    public function getCss()
    {
        return $this->css;
    }

    /**
     * Set is_active
     *
     * @param boolean $isActive
     * @return FormTemplate
     */
    public function setIsActive($isActive)
    {
        $this->is_active = $isActive;

        return $this;
    }

    /**
     * Get is_active
     *
     * @return boolean 
     */
    public function getIsActive()
    {
        return $this->is_active;
    }
    
    public function isActive(){
        if($this->getIsActive())
            return true;
        return false;
    }

    /**
     * Set url
     *
     * @param string $url
     * @return FormTemplate
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string 
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return FormTemplate
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }
    
    public function __toString() {
        return $this->title;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->form = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add form
     *
     * @param \PiZone\FormBundle\Entity\Form $form
     * @return FormTemplate
     */
    public function addForm(\PiZone\FormBundle\Entity\Form $form)
    {
        $this->form[] = $form;

        return $this;
    }

    /**
     * Remove form
     *
     * @param \PiZone\FormBundle\Entity\Form $form
     */
    public function removeForm(\PiZone\FormBundle\Entity\Form $form)
    {
        $this->form->removeElement($form);
    }
    
    /**
     * Get form
     *
     * @return \PiZone\FormBundle\Entity\Form
     */
    public function getForm()
    {
        return $this->form;
    }

    /**
     * Set parent_form
     *
     * @param \PiZone\FormBundle\Entity\Form $parentForm
     * @return FormTemplate
     */
    public function setParentForm(\PiZone\FormBundle\Entity\Form $parentForm = null)
    {
        $this->parent_form = $parentForm;

        return $this;
    }

    /**
     * Get parent_form
     *
     * @return \PiZone\FormBundle\Entity\Form
     */
    public function getParentForm()
    {
        return $this->parent_form;
    }
    
    public function setIsSelected($isSelected)
    {
        $this->is_selected = $isSelected;

        return $this;
    }

    public function getIsSelected()
    {
        return $this->is_selected;
    }
    
    public function isSelected(){
        if($this->getIsSelected())
            return true;
        return false;
    }
    
    /**
     * Set js
     *
     * @param string $js
     * @return FormTemplate
     */
    public function setJs($js)
    {
        $this->js = $js;

        return $this;
    }

    /**
     * Get js
     *
     * @return string 
     */
    public function getJs()
    {
        return $this->js;
    }
}
