<?php

namespace PiZone\FormBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files.
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/configuration.html}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('pi_zone_form');

        $rootNode
            ->children()
            ->arrayNode('uploads')
            ->children()
            ->arrayNode('files')
            ->children()
            ->scalarNode('absolute_path')->isRequired()->cannotBeEmpty()->end()
            ->scalarNode('web_path')->isRequired()->cannotBeEmpty()->end()
            ->end()
            ->end()
            ->end()
            ->end()
            ->end();


        return $treeBuilder;
    }
}
