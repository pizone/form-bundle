function AdminFormFieldNewCtrl($scope, $http, $state, $timeout, SweetAlert, $filter){
    angular.extend($scope, new NewCtrl($scope, $http, $state, $timeout, SweetAlert, $filter));
    $scope.prefix = 'admin/form_field';
    $scope.statelink.list = 'form.field_list';
    $scope.statelink.new = 'form.field_new';
    $scope.statelink.edit = 'form.field_edit';
    $scope.breadcrumbs.title = 'FFIELD.NEW';
    $scope.breadcrumbs.path = [{title: 'FFIELD.LIST', url: 'form.field_list'}];

    var Form = new AdminFormFieldForm($scope, $timeout);

    $scope.Get.Fields = Form.GetFields;

    $scope.Init();
}
AdminFormFieldNewCtrl.$inject = ['$scope', '$http', '$state', '$timeout', 'SweetAlert', '$filter'];
angular.module('PiZone.Form').controller('AdminFormFieldNewCtrl', AdminFormFieldNewCtrl);