function AdminFormFieldListCtrl($scope, $http, $state, $stateParams, SweetAlert, $timeout, $filter, $cookieStore){
    angular.extend($scope, new ListCtrl($scope, $http, $state, $stateParams, SweetAlert, $timeout, $filter, $cookieStore));
    $scope.prefix = 'admin/form_field';
    $scope.statelink.add = 'form.field_new';
    $scope.statelink.edit = 'form.field_edit';
    $scope.statelink.page = 'form.field_list';
    $scope.breadcrumbs.title = 'FFIELD.LIST';
    $scope.model = AdminFormField;
    $scope.columns = [
        {show: true},
        {title: 'ID', name: 'id'},
        {title: 'CONTENT.FIELD.TITLE', name: 'title'},
        {title: 'CONTENT.FIELD.ALIAS', name: 'alias'},
        {title: 'CONTENT.FIELD.IS_ACTIVE.LABEL', name: 'is_active'},
        {title: 'COMMON.ACTIONS'}
    ];
    $scope.showDefaultColumns = {
        all: [0, 1, 2, 3, 4, 5]
    };

    $scope.Callback.GetData = CreateFilters;

    function CreateFilters() {
        var data = $scope.filters;
        data.is_active.template = FieldDispatcher.GetLayout('simple_choice');
        $scope._token.filter = data._token;

        $scope.filters = [
            {icon: 'fa fa-cubes', field: data.all, show: true},
            {icon: 'fa fa-cube', field: data.title, show: data.title.value ? true : false},
            {icon: 'fa fa-codepen', field: data.alias, show: data.alias.value ? true : false},
            {icon: 'fa fa-eye', field: data.is_active, show: data.is_active.value !== null ? true: false},
        ];
    }

    $scope.Init();
}
AdminFormFieldListCtrl.$inject = ['$scope', '$http', '$state', '$stateParams', 'SweetAlert', '$timeout', '$filter', '$cookieStore'];
angular.module('PiZone.Form').controller('AdminFormFieldListCtrl', AdminFormFieldListCtrl);