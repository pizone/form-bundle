function AdminFormFieldEditCtrl($scope, $http, $state, $stateParams, $timeout, SweetAlert, $filter){
    angular.extend($scope, new EditCtrl($scope, $http, $state, $stateParams, $timeout, SweetAlert, $filter));
    $scope.prefix = 'admin/form_field';
    $scope.statelink.list = 'form.field_list';
    $scope.statelink.new = 'form.field_new';
    $scope.breadcrumbs.title = 'FFIELD.EDIT';
    $scope.breadcrumbs.path = [{title: 'FFIELD.LIST', url: 'form.field_list'}];

    var Form = new AdminFormFieldForm($scope, $timeout);

    $scope.Get.Fields = Form.GetFields;

    $scope.Init();
}
AdminFormFieldEditCtrl.$inject = ['$scope', '$http', '$state', '$stateParams', '$timeout', 'SweetAlert', '$filter'];
angular.module('PiZone.Form').controller('AdminFormFieldEditCtrl', AdminFormFieldEditCtrl);