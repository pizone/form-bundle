function AdminFormMessageListCtrl($scope, $http, $state, $stateParams, SweetAlert, $timeout, $filter, $cookieStore){
    angular.extend($scope, new ListCtrl($scope, $http, $state, $stateParams, SweetAlert, $timeout, $filter, $cookieStore));
    $scope.formId = $stateParams.form_id;
    $scope.prefix = 'admin/form_message';
    $scope.statelink.page = 'form.message_list';
    $scope.breadcrumbs.title = 'FMESS.LIST';
    $scope.model = AdminMessageForm;
    $scope.columns = [
        {show: true},
        {title: 'ID', name: 'id'},
        {title: 'FMESS.FIELD.FORM', name: 'form.title'},
        {title: 'FMESS.FIELD.MESS'},
        {title: 'COMMON.ACTIONS'}
    ];
    $scope.showDefaultColumns = {
        all: [0, 1, 2, 3, 4]
    };

    $scope.Callback.GetData = CreateFilters;
    $scope.Get.Layout = GetLayout;


    function GetLayout(){
        var query = $scope.GetBaseQuery() + ($scope.formId ? '?formId=' + $scope.formId : '');
        var params = [];
        if($scope.pageId)
            params.push('page=' + $scope.pageId);
        if($scope.perPage)
            params.push('perPage=' + $scope.perPage);
        if($scope.order.sort && $scope.order.order_by)
            params.push('sort=' + $scope.order.sort + '&order_by=' + $scope.order.order_by);
        if(params.length > 0) {
            if(/\?/.test(query))
                query = query + '&' + params.join('&');
            else
                query = query + '?' + params.join('&');
        }
        $scope.list = [];
        var container = $('#' + $scope.listId);
        var loader = new Loader(container);
        loader.Create();
        $http({
            url: query,
            method: 'POST',
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).then(
            function(response){
                response = JSON.parse(response.data);
                $scope.Callback.GetList(response);
                loader.Delete();
            },
            function(response){
                if (response.status == 400) {
                    response = JSON.parse(response.data);
                    $scope.Callback.GetList(response);
                }
                loader.Delete();
            });
    }

    function CreateFilters() {
        var data = $scope.filters;

        $scope._token.filter = data._token;

        $scope.filters = [
            {icon: 'fa fa-cubes', field: data.all, show: true},
            {icon: 'fa fa-cube', field: data.form, show: data.form.value ? true : false},
            {icon: 'fa fa-cube', field: data.form_field, show: data.form_field.value ? true : false}
        ];
    }

    $scope.Init();
}
AdminFormMessageListCtrl.$inject = ['$scope', '$http', '$state', '$stateParams', 'SweetAlert', '$timeout', '$filter', '$cookieStore'];
angular.module('PiZone.Form').controller('AdminFormMessageListCtrl', AdminFormMessageListCtrl);