function AdminFormEventListCtrl($scope, $http, $state, $stateParams, SweetAlert, $timeout, $filter, $cookieStore){
    angular.extend($scope, new ListCtrl($scope, $http, $state, $stateParams, SweetAlert, $timeout, $filter, $cookieStore));
    $scope.prefix = 'admin/form_event';
    $scope.statelink.add = 'form.event_new';
    $scope.statelink.edit = 'form.event_edit';
    $scope.statelink.page = 'form.event_list';
    $scope.breadcrumbs.title = 'FEVENT.LIST';
    $scope.model = AdminFormEvent;
    $scope.columns = [
        {show: true},
        {title: 'ID', name: 'id'},
        {title: 'FEVENT.FIELD.TITLE', name: 'title'},
        {title: 'FEVENT.FIELD.ALIAS', name: 'alias'},
        {title: 'FEVENT.FIELD.DESCRIPTION', name: 'description'},
        {title: 'FEVENT.FIELD.SUBJECT', name: 'subject'},
        {title: 'FEVENT.FIELD.CONTENT', name: 'content'},
        {title: 'FEVENT.FIELD.IS_ACTIVE.LABEL', name: 'is_active'},
        {title: 'COMMON.ACTIONS'}
    ];
    $scope.showDefaultColumns = {
        all: [0, 1, 2, 3, 4, 7, 8]
    };

    $scope.Callback.GetData = CreateFilters;

    function CreateFilters() {
        var data = $scope.filters;

        data.is_active.template = FieldDispatcher.GetLayout('simple_choice');
        $scope._token.filter = data._token;

        $scope.filters = [
            {icon: 'fa fa-cubes', field: data.all, show: true},
            {icon: 'fa fa-cube', field: data.title, show: data.title.value ? true : false},
            {icon: 'fa fa-cube', field: data.alias, show: data.alias.value ? true : false},
            {icon: 'fa fa-comment', field: data.description, show: data.description.value ? true : false},
            {icon: 'fa fa-codepen', field: data.subject, show: data.subject.value ? true : false},
            {icon: 'fa fa-file', field: data.content, show: data.content.value ? true: false},
            {icon: 'fa fa-eye', field: data.is_active, show: data.is_active.value >= 0 ? true: false},
        ];
    }

    $scope.Init();
}
AdminFormEventListCtrl.$inject = ['$scope', '$http', '$state', '$stateParams', 'SweetAlert', '$timeout', '$filter', '$cookieStore'];
angular.module('PiZone.Form').controller('AdminFormEventListCtrl', AdminFormEventListCtrl);