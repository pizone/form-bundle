function AdminFormEventNewCtrl($scope, $http, $state, $timeout, SweetAlert, $filter){
    angular.extend($scope, new NewCtrl($scope, $http, $state, $timeout, SweetAlert, $filter));
    $scope.prefix = 'admin/form_event';
    $scope.statelink.list = 'form.event_list';
    $scope.statelink.new = 'form.event_new';
    $scope.statelink.edit = 'form.event_edit';
    $scope.breadcrumbs.title = 'FEVENT.NEW';
    $scope.breadcrumbs.path = [{title: 'FEVENT.LIST', url: 'form.event_list'}];

    var Form = new AdminFormEventForm($scope, $timeout);

    $scope.Get.Tabs = Form.GetTabs;

    $scope.Init();
}
AdminFormEventNewCtrl.$inject = ['$scope', '$http', '$state', '$timeout', 'SweetAlert', '$filter'];
angular.module('PiZone.Form').controller('AdminFormEventNewCtrl', AdminFormEventNewCtrl);