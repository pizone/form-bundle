function AdminFormEventEditCtrl($scope, $http, $state, $stateParams, $timeout, SweetAlert, $filter){
    angular.extend($scope, new EditCtrl($scope, $http, $state, $stateParams, $timeout, SweetAlert, $filter));
    $scope.prefix = 'admin/form_event';
    $scope.statelink.list = 'form.event_list';
    $scope.statelink.new = 'form.event_new';
    $scope.breadcrumbs.title = 'FEVENT.EDIT';
    $scope.breadcrumbs.path = [{title: 'FEVENT.LIST', url: 'form.event_list'}];

    var Form = new AdminFormEventForm($scope, $timeout);

    $scope.Get.Tabs = Form.GetTabs;

    $scope.Init();
}
AdminFormEventEditCtrl.$inject = ['$scope', '$http', '$state', '$stateParams', '$timeout', 'SweetAlert', '$filter'];
angular.module('PiZone.Form').controller('AdminFormEventEditCtrl', AdminFormEventEditCtrl);