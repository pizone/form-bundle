function AdminFormStatusListCtrl($scope, $http, $state, $stateParams, SweetAlert, $timeout, $filter, $cookieStore){
    angular.extend($scope, new ListCtrl($scope, $http, $state, $stateParams, SweetAlert, $timeout, $filter, $cookieStore));
    $scope.prefix = 'admin/form_status';
    $scope.statelink.add = 'form.status_new';
    $scope.statelink.edit = 'form.status_edit';
    $scope.statelink.page = 'form.status_list';
    $scope.breadcrumbs.title = 'FSTATUS.LIST';
    $scope.model = AdminFormStatus;
    $scope.columns = [
        {show: true},
        {title: 'ID', name: 'id'},
        {title: 'FSTATUS.FIELD.TITLE', name: 'title'},
        {title: 'FSTATUS.FIELD.ALIAS', name: 'alias'},
        {title: 'FSTATUS.FIELD.SORT', name: 'sort'},
        {title: 'FSTATUS.FIELD.IS_ACTIVE.LABEL', name: 'is_active'},
        {title: 'COMMON.ACTIONS'}
    ];
    $scope.showDefaultColumns = {
        all: [0, 1, 2, 3, 4, 5, 6]
    };

    $scope.Callback.GetData = CreateFilters;

    function CreateFilters() {
        var data = $scope.filters;

        data.is_active.template = FieldDispatcher.GetLayout('simple_choice');
        $scope._token.filter = data._token;

        $scope.filters = [
            {icon: 'fa fa-cubes', field: data.all, show: true},
            {icon: 'fa fa-cube', field: data.title, show: data.title.value ? true : false},
            {icon: 'fa fa-cubes', field: data.alias, show: data.alias.value ? true : false},
            {icon: 'fa fa-sort-numeric-asc', field: data.sort, show: data.sort.value ? true: false},
            {icon: 'fa fa-eye', field: data.is_active, show: data.is_active.value >= 0 ? true: false},
        ];
    }

    $scope.Init();
}
AdminFormStatusListCtrl.$inject = ['$scope', '$http', '$state', '$stateParams', 'SweetAlert', '$timeout', '$filter', '$cookieStore'];
angular.module('PiZone.Form').controller('AdminFormStatusListCtrl', AdminFormStatusListCtrl);