function AdminFormStatusNewCtrl($scope, $http, $state, $timeout, SweetAlert, $filter){
    angular.extend($scope, new NewCtrl($scope, $http, $state, $timeout, SweetAlert, $filter));
    $scope.prefix = 'admin/form_status';
    $scope.statelink.list = 'form.status_list';
    $scope.statelink.new = 'form.status_new';
    $scope.statelink.edit = 'form.status_edit';
    $scope.breadcrumbs.title = 'FSTATUS.NEW';
    $scope.breadcrumbs.path = [{title: 'FSTATUS.LIST', url: 'form.status_list'}];

    var Form = new AdminFormStatusForm($scope);

    $scope.Get.Fields = Form.GetFields;

    $scope.Init();
}
AdminFormStatusNewCtrl.$inject = ['$scope', '$http', '$state', '$timeout', 'SweetAlert', '$filter'];
angular.module('PiZone.Form').controller('AdminFormStatusNewCtrl', AdminFormStatusNewCtrl);