function AdminFormStatusEditCtrl($scope, $http, $state, $stateParams, $timeout, SweetAlert, $filter){
    angular.extend($scope, new EditCtrl($scope, $http, $state, $stateParams, $timeout, SweetAlert, $filter));
    $scope.prefix = 'admin/form_status';
    $scope.statelink.list = 'form.status_list';
    $scope.statelink.new = 'form.status_new';
    $scope.breadcrumbs.title = 'FSTATUS.EDIT';
    $scope.breadcrumbs.path = [{title: 'FSTATUS.LIST', url: 'form.field_list'}];

    var Form = new AdminFormStatusForm($scope);

    $scope.Get.Fields = Form.GetFields;

    $scope.Init();
}
AdminFormStatusEditCtrl.$inject = ['$scope', '$http', '$state', '$stateParams', '$timeout', 'SweetAlert', '$filter'];
angular.module('PiZone.Form').controller('AdminFormStatusEditCtrl', AdminFormStatusEditCtrl);