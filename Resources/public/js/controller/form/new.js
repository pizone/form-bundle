function AdminFormNewCtrl($scope, $http, $state, $timeout, SweetAlert, $filter, $modal){
    angular.extend($scope, new NewCtrl($scope, $http, $state, $timeout, SweetAlert, $filter));
    $scope.prefix = 'admin/form';
    $scope.statelink.list = 'form.form_list';
    $scope.statelink.new = 'form.form_new';
    $scope.statelink.edit = 'form.form_edit';
    $scope.breadcrumbs.title = 'FFORM.NEW';
    $scope.breadcrumbs.path = [{title: 'FFORM.LIST', url: 'form.form_list'}];

    $scope.fieldCollectionPrototype = null;
    $scope.tmplCollectionPrototype = null;
    $scope.collectionIndex = 0;
    $scope.collectionTemplIndex = 0;

    var Form = new AdminFormForm($scope, $timeout, $modal, $filter, SweetAlert);

    $scope.Get.Tabs = Form.GetTabs;
    $scope.Get.Values = GetValues;

    function GetValues(){
        $scope.formData = new FormData();
        var data =  $('#' + $scope.formId).serializeArray();

        for (var i = 0; i <= data.length - 1; i++) {
            $scope.formData.append(data[i].name, data[i].value);
        }

        return $scope.formData;
    }

    $scope.Init();
}
AdminFormNewCtrl.$inject = ['$scope', '$http', '$state', '$timeout', 'SweetAlert', '$filter', '$modal'];
angular.module('PiZone.Form').controller('AdminFormNewCtrl', AdminFormNewCtrl);