function AdminFormEditCtrl($scope, $http, $state, $stateParams, $timeout, SweetAlert, $filter, $modal){
    angular.extend($scope, new EditCtrl($scope, $http, $state, $stateParams, $timeout, SweetAlert, $filter));
    $scope.prefix = 'admin/form';
    $scope.statelink.list = 'form.form_list';
    $scope.statelink.new = 'form.form_new';
    $scope.breadcrumbs.title = 'FFORM.EDIT';
    $scope.breadcrumbs.path = [{title: 'FFORM.LIST', url: 'form.form_list'}];

    $scope.fieldCollectionPrototype = null;
    $scope.tmplCollectionPrototype = null;
    $scope.collectionIndex = 0;
    $scope.collectionTemplIndex = 0;

    var Form = new AdminFormForm($scope, $timeout, $modal, $filter, SweetAlert);

    $scope.Get.Tabs = Form.GetTabs;
    $scope.Get.Values = GetValues;

    function GetValues(){
        $scope.formData = new FormData();
        var data =  $('#' + $scope.formId).serializeArray();

        data = data.concat(
            $('#' + $scope.formId + ' input[id^=form_form_fields]').map(
                function() {
                    if($(this).attr('checked')) {
                        return {"name": this.name, "value": this.value};
                    }
                }).get()
        );

        for (var i = 0; i <= data.length - 1; i++) {
            $scope.formData.append(data[i].name, data[i].value);
        }

        return $scope.formData;
    }

    $scope.Init();
}
AdminFormEditCtrl.$inject = ['$scope', '$http', '$state', '$stateParams', '$timeout', 'SweetAlert', '$filter', '$modal'];
angular.module('PiZone.Form').controller('AdminFormEditCtrl', AdminFormEditCtrl);