function ModalFormTmplEditCtrl($scope, $http, $state, $stateParams, $timeout, SweetAlert, $filter, $modalInstance, form, callback){
    angular.extend($scope, new EditCtrl($scope, $http, $state, $stateParams, $timeout, SweetAlert, $filter));

    $scope.form = form;
    $scope.callback = callback;

    $scope.Click.Cancel = ClickCancel;
    $scope.Click.Submit = ClickSubmit;
    $scope.Get.Layout = GetLayout;

    function GetLayout(){
        $scope.tabs = $scope.form;
    }

    function ClickSubmit(){
        if($scope.IsValid()) {
            $modalInstance.close();
            $scope.callback($scope.form);
            $scope.ViewNotify('submit', 'success');
        }
    }

    function ClickCancel(){
        $modalInstance.close();
    }

    $scope.Init();
}
ModalFormTmplEditCtrl.$inject = ['$scope', '$http', '$state', '$stateParams', '$timeout', 'SweetAlert', '$filter', '$modalInstance', 'form', 'callback'];
angular.module('PiZone.Form').controller('ModalFormTmplEditCtrl', ModalFormTmplEditCtrl);