function AdminFormListCtrl($scope, $http, $state, $stateParams, SweetAlert, $timeout, $filter, $cookieStore){
    angular.extend($scope, new ListCtrl($scope, $http, $state, $stateParams, SweetAlert, $timeout, $filter, $cookieStore));
    $scope.prefix = 'admin/form';
    $scope.statelink.add = 'form.form_new';
    $scope.statelink.edit = 'form.form_edit';
    $scope.statelink.page = 'form.form_list';
    $scope.breadcrumbs.title = 'FFORM.LIST';
    $scope.model = AdminForm;
    $scope.columns = [
        {show: true},
        {title: 'ID', name: 'id'},
        {title: 'FEVENT.FIELD.TITLE', name: 'title'},
        {title: 'FEVENT.FIELD.ALIAS', name: 'alias'},
        {title: 'FEVENT.FIELD.DESCRIPTION', name: 'description'},
        {title: 'FEVENT.FIELD.IS_ACTIVE.LABEL', name: 'is_active'},
        {title: 'COMMON.ACTIONS'}
    ];
    $scope.showDefaultColumns = {
        all: [0, 1, 2, 3, 4, 5, 6]
    };

    $scope.Callback.GetData = CreateFilters;

    function CreateFilters() {
        var data = $scope.filters;

        data.is_active.template = FieldDispatcher.GetLayout('simple_choice');
        $scope._token.filter = data._token;

        $scope.filters = [
            {icon: 'fa fa-cubes', field: data.all, show: true},
            {icon: 'fa fa-cube', field: data.title, show: data.title.value ? true : false},
            {icon: 'fa fa-cube', field: data.alias, show: data.alias.value ? true : false},
            {icon: 'fa fa-comment', field: data.description, show: data.description.value ? true : false},
            {icon: 'fa fa-eye', field: data.is_active, show: data.is_active.value >= 0 ? true: false},
        ];
    }

    $scope.Init();
}
AdminFormListCtrl.$inject = ['$scope', '$http', '$state', '$stateParams', 'SweetAlert', '$timeout', '$filter', '$cookieStore'];
angular.module('PiZone.Form').controller('AdminFormListCtrl', AdminFormListCtrl);