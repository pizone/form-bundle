function AdminFormForm($scope, $timeout, $modal, $filter, SweetAlert) {
    var self = this;
    self.GetTabs = GetTabs;

    $scope.ClickAddField = ClickAddField;
    $scope.ClickRemoveField = ClickRemoveField;
    $scope.ClickEditField = ClickEditField;
    $scope.ClickActiveField = ClickActiveField;
    $scope.ClickAddTmpl = ClickAddTmpl;
    $scope.ClickRemoveTmpl = ClickRemoveTmpl;
    $scope.ClickEditTmpl = ClickEditTmpl;
    $scope.ClickEditEvent = ClickEditEvent;
    $scope.ClickCloneDefTmpl = ClickCloneDefTmpl;
    $scope.ClickCloneTmpl = ClickCloneTmpl;
    $scope.Befor.Submit = BeforSubmit;

    $scope.testIssetField = false;
    $scope.testIssetTmpl = false;
    $scope.selectedTmpl = {
        key: ''
    };

    function GetTabs(data){
        $scope.breadcrumbs.param = data.alias.value;

        var requiredMess = 'FFORM.MESSAGE.REQUIRED.';

        $scope.fieldCollectionPrototype = data.form_fields.prototype;
        $scope.tmplCollectionPrototype = data.form_templates.prototype;
        data.form_fields.template = '/bundles/pizoneform/tmpl/form/_fields_collection.html';
        data.form_templates.template = '/bundles/pizoneform/tmpl/form/_tmpl_collection.html';
        data.events.template = '/bundles/pizoneform/tmpl/form/_events.html';
        data.form_event.template = '/bundles/pizoneform/tmpl/form/_form_event.html';

        $scope.tabs = [
            {
                title: 'FFORM.TAB.GENERAL.TITLE',
                description: '',
                form: true,
                valid: true,
                groups: [
                    [
                        {size: 0, icon: '', field: data._token},
                        {size: 4, icon: 'fa fa-eye', field: data.is_active}
                    ],
                    [
                        {
                            size: 6, icon: 'fa fa-cube', field: data.title, assert: [
                            {key: 'notNull', message: requiredMess + 'TITLE'}
                        ]
                        },
                        {
                            size: 6, icon: 'fa fa-codepen', field: data.alias, assert: [
                            {key: 'notNull', message: requiredMess + 'ALIAS'}
                        ]
                        }
                    ],
                    [
                        {size: 12, icon: 'fa fa-comment', field: data.description}
                    ],
                    [
                        {size: 12, icon: 'fa fa-cloud', field: data.domains}
                    ]
                ]
            },
            {
                title: 'FFORM.TAB.FIELDS.TITLE',
                description: '',
                form: true,
                valid: true,
                groups: [
                    [
                        {size: 12, icon: 'fa fa-file-code-o', field: GetCollectionForm(data.form_fields)}
                    ]
                ]
            },
            {
                title: 'FFORM.TAB.TMPL.TITLE',
                description: '',
                form: true,
                valid: true,
                groups: [
                    [
                        {size: 12, icon: 'fa fa-file-code-o', field: GetCollectionTemplateForm(data.form_templates)},
                        {size: 0, icon: '', field: data.template}
                    ]
                ]
            },
            {
                title: 'FFORM.TAB.EVENTS.TITLE',
                description: '',
                form: true,
                valid: true,
                groups: [
                    [
                        {size: 12, icon: '', field: data.events},
                        {size: 12, icon: '', field: data.form_event, options: {validate: false}}
                    ]
                ]
            },
            {
                title: 'FFORM.TAB.MAIL_TO.TITLE',
                description: '',
                form: true,
                valid: true,
                groups: [
                    [
                        {size: 12, icon: 'fa fa-at', field: data.mail_to}
                    ]
                ]
            }
            ];

        GetEventForm(data.form_event.form);

        $scope.$watch('selectedTmpl', function(val){
            var t = $scope.tabs[2].groups[0][0].field.form;
            for(var key in t){
                t[key][0].groups[0][3].field.value = 0;
            }
            if(val.key !== ''){
                $scope.tabs[2].groups[0][0].field.form[val.key][0].groups[0][3].field.value = 1;
            }
        }, true);

        var tmpl = $scope.tabs[2].groups[0][0].field.form;
        for(var key in tmpl){
            if(parseInt(tmpl[key][0].groups[0][3].field.value) === 1)
                $scope.selectedTmpl.key = key;
        }
    }

    function pad (str, max) {
        str = str.toString();
        return str.length < max ? pad("0" + str, max) : str;
    }

    function GetCollectionForm(codes){
        var collection = {};
        if(codes.form.length !== 0) {
            $.each(codes.form, function(i){
                collection[pad($scope.collectionIndex, 3)] = GetFieldsForm(codes.form[i].form);
                $scope.collectionIndex = $scope.collectionIndex + 1;
            });
            codes.form = collection;
            $scope.testIssetField = true;
        }

        return codes;
    }

    function GetCollectionTemplateForm(codes){
        var collection = {};
        if(codes.form.length !== 0) {
            $.each(codes.form, function(i){
                collection[pad($scope.collectionTemplIndex, 3)] = GetTmplForm(codes.form[i].form);
                $scope.collectionTemplIndex = $scope.collectionTemplIndex + 1;
            });
            codes.form = collection;
            $scope.testIssetTmpl = true;
        }

        return codes;
    }

    function GetNewCollectionForm() {
        var form = angular.copy($scope.fieldCollectionPrototype);

        $.each(form, function (i) {
            $.each(form[i], function (j) {
                if (typeof(form[i][j]) == 'string')
                    form[i][j] = form[i][j].replace('__name__', $scope.collectionIndex);
            });
        });

        return GetFieldsForm(form);
    }

    function GetNewTmplCollectionForm() {
        var form = angular.copy($scope.tmplCollectionPrototype);

        $.each(form, function (i) {
            $.each(form[i], function (j) {
                if (typeof(form[i][j]) == 'string')
                    form[i][j] = form[i][j].replace('__name__', $scope.collectionTemplIndex);
            });
        });

        return GetTmplForm(form);
    }

    function ClickAddField(){
        $timeout(function(){
            var form = GetNewCollectionForm();

            $modal.open({
                templateUrl: '/bundles/pizoneform/tmpl/form/_edit_field.html',
                size: 'lg',
                controller: ModalFormFieldEditCtrl,
                resolve: {
                    form: function(){
                        return form;
                    },
                    callback: function(){
                        return function(form){
                            if($scope.collectionIndex === 0)
                                $scope.tabs[1].groups[0][0].field.form = {};
                            $scope.tabs[1].groups[0][0].field.form[pad($scope.collectionIndex, 3)] = form;
                            $scope.collectionIndex = $scope.collectionIndex + 1;

                            $scope.testIssetField = true;
                        };
                    }
                }
            });
        });
    }

    function ClickAddTmpl(){
        $timeout(function(){
            var form = GetNewTmplCollectionForm();

            $modal.open({
                templateUrl: '/bundles/pizoneform/tmpl/form/_edit_tmpl.html',
                size: 'lg',
                controller: ModalFormTmplEditCtrl,
                resolve: {
                    form: function(){
                        return form;
                    },
                    callback: function(){
                        return function(form){
                            if($scope.collectionTemplIndex === 0)
                                $scope.tabs[2].groups[0][0].field.form = {};
                            $scope.tabs[2].groups[0][0].field.form[pad($scope.collectionTemplIndex, 3)] = form;
                            $scope.collectionTemplIndex = $scope.collectionTemplIndex + 1;

                            $scope.testIssetTmpl = true;
                        };
                    }
                }
            });
        });
    }

    function ClickRemoveField(key){
        function translate(key){
            return $filter('translate')($scope.messages.remove.confirm[key]);
        }
        SweetAlert.swal({
                title: translate('title'),
                text: translate('message'),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: translate('confirmButton'),
                cancelButtonText: translate('cancelButton'),
                closeOnConfirm: false,
                closeOnCancel: false },
            function (isConfirm) {
                if (isConfirm) {
                    var collection =  $scope.tabs[1].groups[0][0].field.form,
                        form = {};
                    delete collection[key];

                    $.each(collection, function(i, one){
                        if(one)
                            form[i] = one;
                    });
                    $scope.tabs[1].groups[0][0].field.form= form;

                    if(Object.keys(form).length === 0)
                        $scope.testIssetField = false;
                } else {
                    SweetAlert.swal(translate('cancelledTitle'), translate('cancelledMessage'), "error");
                }
            });
    }

    function ClickRemoveTmpl(key){
        function translate(key){
            return $filter('translate')($scope.messages.remove.confirm[key]);
        }
        SweetAlert.swal({
                title: translate('title'),
                text: translate('message'),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: translate('confirmButton'),
                cancelButtonText: translate('cancelButton'),
                closeOnConfirm: false,
                closeOnCancel: false },
            function (isConfirm) {
                if (isConfirm) {
                    var collection =  $scope.tabs[2].groups[0][0].field.form,
                        form = {};
                    delete collection[key];

                    $.each(collection, function(i, one){
                        if(one)
                            form[i] = one;
                    });
                    $scope.tabs[2].groups[0][0].field.form= form;

                    if(Object.keys(form).length === 0)
                        $scope.testIssetTmpl = false;
                } else {
                    SweetAlert.swal(translate('cancelledTitle'), translate('cancelledMessage'), "error");
                }
            });
    }

    function ClickEditField(key){
        var form = $scope.tabs[1].groups[0][0].field.form[key];

        $modal.open({
            templateUrl: '/bundles/pizoneform/tmpl/form/_edit_field.html',
            size: 'lg',
            controller: ModalFormFieldEditCtrl,
            resolve: {
                form: function(){
                    return form;
                },
                callback: function(){
                    return function(form){
                        $scope.tabs[1].groups[0][0].field.form[key] = form;
                    };
                }
            }
        });
    }

    function ClickEditTmpl(key){
        var form = $scope.tabs[2].groups[0][0].field.form[key];

        $modal.open({
            templateUrl: '/bundles/pizoneform/tmpl/form/_edit_tmpl.html',
            size: 'lg',
            controller: ModalFormTmplEditCtrl,
            resolve: {
                form: function(){
                    return form;
                },
                callback: function(){
                    return function(form){
                        $scope.tabs[2].groups[0][0].field.form[key] = form;
                    };
                }
            }
        });
    }

    function ClickCloneDefTmpl(){

    }

    function ClickCloneTmpl(key){

    }

    function BeforSubmit(){
        var events = $scope.tabs[3].groups[0][1].field.form;
        for(var i in events){
            if(!events[i][0][1].field.value && !events[i][1][0].field.value) {
                $('#' + events[i][0][0].field.id).attr('disabled', 'disabled');
                $('#' + events[i][0][1].field.id).attr('disabled', 'disabled');
                $('#' + events[i][1][0].field.id).attr('disabled', 'disabled');
            }
            else{
                $('#' + events[i][0][0].field.id).removeAttr('disabled');
                $('#' + events[i][0][1].field.id).removeAttr('disabled');
                $('#' + events[i][1][0].field.id).removeAttr('disabled');
            }
        }
    }

    function GetEventForm(data){
        var form = [];
        var requiredMess = 'FFIELD.MESSAGE.REQUIRED.';

        for(var j in data){
            form.push([
                [
                    {size: 0, icon: '', field: data[j].form.event},
                    {size: 12, icon: 'fa fa-cube', field: data[j].form.subject, assert: [
                        {key: 'notNull', message: requiredMess  + 'SUBJECT'}
                    ]}
                ],
                [
                    {size: 12, icon: 'fa fa-file', field: data[j].form.content, assert: [
                        {key: 'notNull', message: requiredMess  + 'CONTENT'}
                    ]}
                ]
            ]);
        }
        $scope.tabs[3].groups[0][1].field.form = form;
    }

    function ClickEditEvent(key){
        var events = $scope.tabs[3].groups[0][1].field.form,
            form;
        for(var j in events){
            if(events[j][0][0].field.value === parseInt(key)) {
                form = events[j];
                break;
            }
        }

        $modal.open({
            size: 'lg',
            templateUrl: '/bundles/pizoneform/tmpl/form/_modal_form_event.html',
            controller: ModalFormEventEditCtrl,
            resolve: {
                form: function(){
                    return form;
                },
                callback: function(){
                    return function(form){
                        console.log(form);
                        console.log($scope.tabs[3].groups[0][1].field.form);
                    };
                }
            }
        });
    }

    function ClickActiveField(key, test){
        $scope.tabs[1].groups[0][0].field.form[key][0].groups[0][1].field.checked = test;
    }

    function GetFieldsForm(data){
        var requiredMess = 'FFORM.MESSAGE.REQUIRED.';
        return [
            {
                title: 'FFORM.TAB.MAIN.TITLE',
                description: '',
                form: true,
                valid: true,
                groups: [
                    [
                        {size: 0, icon: '', field: data.id ? data.id : {valid: true}},
                        {size: 12, icon: 'fa fa-eye', field: data.is_active},
                        {size: 12, icon: 'fa fa-code', field: data.field, assert: [
                            {key: 'notNull', message: requiredMess + 'FIELD'}
                        ]},
                        {size: 6, icon: 'fa fa-cube', field: data.title, assert: [
                            {key: 'notNull', message: requiredMess + 'TITLE'}
                        ]},
                        {size: 6, icon: 'fa fa-codepen', field: data.alias, assert: [
                            {key: 'notNull', message: requiredMess + 'ALIAS'}
                        ]},
                        {size: 12, icon: 'fa fa-cubes', field: data.default_value}
                    ]
                ]
            },
            {
                title: 'FFORM.TAB.ATTR.TITLE',
                description: '',
                form: true,
                valid: true,
                groups: [
                    [
                        {size: 6, icon: 'fa fa-terminal', field: data.placeholder},
                        {size: 6, icon: 'fa fa-code', field: data.class},
                        {size: 12, icon: 'fa fa-cubes', field: data.attr}
                    ]
                ]
            },
            {
                title: 'FFORM.TAB.VALIDATION.TITLE',
                description: '',
                form: true,
                valid: true,
                groups: [
                    [
                        {size: 12, icon: 'fa fa-exclamation-triangle', field: data.is_required},
                        {size: 12, icon: 'fa fa-code', field: data.regexp_pattern}
                    ]
                ]
            },
            {
                title: 'FFORM.TAB.MORE.TITLE',
                description: '',
                form: true,
                valid: true,
                groups: [
                    [
                        {size: 12, icon: 'fa fa-sort-numeric-asc', field: data.sort}
                    ]
                ]
            }
        ];
    }

    function GetTmplForm(data){

        var requiredMess = 'FFORM.MESSAGE.REQUIRED.';
        return [
            {
                title: 'FFORM.TAB.MAIN.TITLE',
                description: '',
                form: true,
                valid: true,
                groups: [
                    [
                        {size: 0, icon: '', field: data.id ? data.id : {valid: true}},
                        {size: 12, icon: 'fa fa-eye', field: data.is_active},
                        {size: 12, icon: 'fa fa-cube', field: data.title, assert: [
                            {key: 'notNull', message: requiredMess + 'TITLE'}
                        ]},
                        {size: 0, icon: '', field: data.is_selected}
                    ]
                ]
            },
            {
                title: 'FFORM.TAB.TMPL.TITLE',
                description: '',
                form: true,
                valid: true,
                groups: [
                    [
                        {size: 12, icon: 'fa fa-file', field: data.template, assert: [
                            {key: 'notNull', message: requiredMess + 'TEMPL'}
                        ]}
                    ]
                ]
            },
            {
                title: 'FFORM.TAB.CSS.TITLE',
                description: '',
                form: true,
                valid: true,
                groups: [
                    [
                        {size: 12, icon: 'fa fa-code', field: data.css}
                    ]
                ]
            },
            {
                title: 'FFORM.TAB.JS.TITLE',
                description: '',
                form: true,
                valid: true,
                groups: [
                    [
                        {size: 12, icon: 'fa fa-code', field: data.js}
                    ]
                ]
            }
        ];
    }
}