function AdminForm($scope, $http, $state, i, data, SweetAlert, $filter){
    var self = new Model($scope, $http, $state, i, data, SweetAlert, $filter);
    self.prefix = 'admin/form';
    self.statelink.edit = 'form.form_edit';
    self.Click.ListMessages = ClickListMessages;

    function ClickListMessages(){
        $state.go('form.message_list', {form_id: self.id});
    }

    angular.extend(this, self);
}