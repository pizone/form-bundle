function AdminFormStatusForm($scope) {
    var self = this;
    self.GetFields = GetFields;

    function GetFields(data){
        $scope.breadcrumbs.param = data.title.value;

        var requiredMess = 'FSTATUS.MESSAGE.REQUIRED.';

        $scope.fields =
            [
                [
                    {size: 0, icon: '', field: data._token},
                    {size: 8, icon: 'fa fa-eye', field: data.is_active}
                ],
                [
                    {size: 8, icon: 'fa fa-cube', field: data.title, assert: [
                        {key: 'notNull', message: requiredMess  + 'TITLE'}
                    ]}
                ],
                [
                    {size: 8, icon: 'fa fa-cubes', field: data.alias, assert: [
                        {key: 'notNull', message: requiredMess  + 'ALIAS'}
                    ]}
                ],
                [
                    {size: 8, icon: 'fa fa-sort-numeric-asc', field: data.sort}
                ]
            ];
    }
}