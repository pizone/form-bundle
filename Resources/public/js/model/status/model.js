function AdminFormStatus($scope, $http, $state, i, data, SweetAlert, $filter){
    var self = new Model($scope, $http, $state, i, data, SweetAlert, $filter);
    self.prefix = 'admin/form_status';
    self.statelink.edit = 'form.status_edit';

    angular.extend(this, self);
}