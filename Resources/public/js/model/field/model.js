function AdminFormField($scope, $http, $state, i, data, SweetAlert, $filter){
    var self = new Model($scope, $http, $state, i, data, SweetAlert, $filter);
    self.prefix = 'admin/form_field';
    self.statelink.edit = 'form.field_edit';

    angular.extend(this, self);
}