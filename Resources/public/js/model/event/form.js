function AdminFormEventForm($scope, $timeout) {
    var self = this;
    self.GetTabs = GetTabs;

    function GetTabs(data){
        $scope.breadcrumbs.param = data.alias.value;

        var requiredMess = 'FEVENT.MESSAGE.REQUIRED.';

        $scope.tabs = [
            {
                title: 'CONTENT.TAB.GENERAL.TITLE',
                description: '',
                form: true,
                valid: true,
                groups: [
                    [
                        {size: 0, icon: '', field: data._token},
                        {size: 4, icon: 'fa fa-eye', field: data.is_active}
                    ],
                    [
                        {
                            size: 6, icon: 'fa fa-cube', field: data.title, assert: [
                            {key: 'notNull', message: requiredMess + 'TITLE'}
                        ]
                        },
                        {
                            size: 6, icon: 'fa fa-codepen', field: data.alias, assert: [
                            {key: 'notNull', message: requiredMess + 'ALIAS'}
                        ]
                        }
                    ],
                    [
                        {size: 12, icon: 'fa fa-comment', field: data.description}
                    ]
                ]
            },
            {
                title: 'CONTENT.TAB.CONTENT.TITLE',
                description: '',
                form: true,
                valid: true,
                groups: [
                    [
                        {
                            size: 12, icon: 'fa fa-codepen', field: data.subject, assert: [
                                {key: 'notNull', message: requiredMess + 'SUBJECT'}
                            ]
                        }
                    ],
                    [
                        {
                            size: 12, icon: 'fa fa-file', field: data.content, assert: [
                                {key: 'notNull', message: requiredMess + 'CONTENT'}
                            ]
                        }
                    ]
                ]
            }
            ];
    }
}