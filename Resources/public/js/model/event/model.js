function AdminFormEvent($scope, $http, $state, i, data, SweetAlert, $filter){
    var self = new Model($scope, $http, $state, i, data, SweetAlert, $filter);
    self.prefix = 'admin/form_event';
    self.statelink.edit = 'form.event_edit';

    angular.extend(this, self);
}