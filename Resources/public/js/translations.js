function FormTranslationConfig($translateProvider) {
    $translateProvider
        .translations('en', {
            FFIELD: enFormFieldPZTranslates,
            FSTATUS: enFormStatusPZTranslates,
            FEVENT: enFormEventPZTranslates,
            FFORM: enFormPZTranslates,
            FMESS: enFormMessPZTranslates
        })
        .translations('ru', {
            FFIELD: ruFormFieldPZTranslates,
            FSTATUS: ruFormStatusPZTranslates,
            FEVENT: ruFormEventPZTranslates,
            FFORM: ruFormPZTranslates,
            FMESS: ruFormMessPZTranslates
        });
}
FormTranslationConfig.$inject = ['$translateProvider'];

angular
    .module('PiZone.Form')
    .config(FormTranslationConfig);
