var JSConfig = {
    api : {
        url: '/api/client/form_message/',
        getJS : 'get_js/',
        getCSS : 'get_css/',
        getTmpl : 'get_template/'
    },
    js : [],
    css : []
};

var EventDispatcher = {
    events : [],
    
    AddEventListener : function (event, callback) {
        this.events[event] = this.events[event] || [];
        if (this.events[event]) {
            this.RemoveEventListener(event, callback);
            this.events[event].push(callback);
        }
    },

    RemoveEventListener : function (event, callback) {
        if (this.events[event]) {
            var listeners = this.events[event];
            var callbackHash = EventDispatcher.HashCode(callback.toString());
            for (var i = listeners.length - 1; i >= 0; --i) {
                if (EventDispatcher.HashCode(listeners[i].toString()) === callbackHash) {
                    listeners.splice(i, 1);
                    return true;
                }
            }
        }
        return false;
    },

    DispatchEvent: function (event, data) {
        if (this.events[event]) {
            var listeners = this.events[event], len = listeners.length;
            while (len--) {
                listeners[len](data);   //callback with self
            }
        }
    },
    
    HashCode : function(str){
        var hash = 0, i, ch;
        if (str.length === 0) return hash;
        for (i = 0; i < str.length; i++) {
            ch = str.charCodeAt(i);
            hash = ((hash<<5)-hash)+ch;
            hash = hash & hash; // Convert to 32bit integer
        }
        return hash;
    }
};

var JSLoader = function(){
    var self = this;
    self.version = '1.0';
    self.id = null;
    self.loaded = false;
    self.loadedCount = 0;
    self.pathToJs = null;
    self.dev = false;
    self.Init = function(id, dev){
        self.id = id;
        var all = JSConfig.js.length;
        self.dev = dev;

        self.LoadScript(JSConfig.js, function(){self.RegisterLoaded(all);});
        self.LoadCss(JSConfig.css);
    };
    self.RegisterLoaded = function(all){
        self.loadedCount = self.loadedCount + 1;
        if (self.loadedCount == all){
            self.OnReady();
        }
    };
    self.OnReady = function(){
        self.loaded = true;
        EventDispatcher.DispatchEvent('onload.WfScripts_' + self.id);
    };
    self.LoadScript = function(scripts, callback) {
        var head = document.getElementsByTagName("head")[0] || document.documentElement;

        function CreateScript(head, src){
            var script = document.createElement("script");
            script.async = true;
            script.type = 'text/javascript';

            script.src = (self.dev ? '/app_dev.php' : '') + JSConfig.api.url + JSConfig.api.getJS + src + '?v=' + self.version;

            script.async = true;

            if (script.readyState) { //IE
                script.onreadystatechange = function () {
                    if (script.readyState == "loaded" || script.readyState == "complete") {
                        script.onreadystatechange = null;
                        if(callback)
                            callback();
                    }
                };
            }
            else { // Non IE
                script.onload = function () {
                    if(callback)
                        script.onload = callback();
                };
            }

            head.appendChild(script);
        }

        for(var i in scripts){
            CreateScript(head, scripts[i]);
        }

    };
    self.LoadCss = function(css) {
        var head = document.getElementsByTagName("head")[0] || document.documentElement;

        for(var i in css){
            var link = document.createElement("link");
            link.type = "text/css";
            link.rel = "stylesheet";
            link.href = (self.dev ? '/app_dev.php' : '') + JSConfig.api.url + JSConfig.api.getCSS + css[i] + '?v=' + self.version;

            head.appendChild(link);
        }
    };
};

var WfForm = function() {
    var self = this;
    self.form = null;
    self.proxy = null;
    self.container = null;
    self.redirect_url = null;
    self.callback = null;
    self.dev = false;
    
    self.SetOptions = function(option) {
        if (option.form)
            self.form = option.form;
        if (option.container)
            self.container = option.container;
        if (option.api)
            JSConfig.api.url = option.api;
        if (option.redirect_url)
            self.redirect_url = option.redirect_url;
        if (option.proxy)
            self.proxy = option.proxy;
        if(option.callback)
            self.callback = option.callback;
        if (option.dev)
            self.dev = true;
    };
    self.Init = {
        Main: function(callback){
            if(typeof(jQuery) != 'undefined'){
                self.Set.Container();
                self.Set.CustomCSS();
                self.Set.CustomJS();
                EventDispatcher.AddEventListener('onload.WfScripts_' + self.form, function (){
                    callback();
                });
                var loader = new JSLoader();
                loader.Init(self.form, self.dev);
            }
            else
                setTimeout(function(){self.Init.Main(callback);}, 100);
        },
        Mask: function(){
            $('#' + self.form + '_text_phone').mask("+7-999-999-99-99", {placeholder: "_"});
        },
        Placeholder: function(){
            $('input, textarea').placeholder();
        },
        Form: function() {
            self.Ajax.Submit.Form();
            self.Init.Mask();
            self.Init.Placeholder();
            EventDispatcher.DispatchEvent('onexecute.WfScripts_' + self.form);
        },
        Component: function() {
            if (self.form) {
                self.Init.Main(function(){
                    self.Init.Form();
                });
            }
        },
        Module: function() {
            if (self.form && self.container) {
                self.Init.Main(function(){
                    self.Ajax.DownloadTemplate();
                });
            }
        }
    };
    self.Ajax = {
        DownloadTemplate: function() {
            $.ajax({
                url: (self.dev ? '/app_dev.php' : '') + JSConfig.api.url + JSConfig.api.getTmpl + self.form,
                jsonp: 'callback',
                dataType: 'jsonp',
                timeout: 30000,
                success: function(msg) {
                    self.container.html(msg.template);
                    self.Init.Form();
                }
            });
        },
        Submit: {
            Form : function(){
                $('#wf_form_' + self.form).one('submit', function() {
                    if(self.proxy)
                        self.Ajax.Submit.Proxy();
                    else
                        self.Ajax.Submit.Direct();
                    
                    return false;
                });
            },
            Direct : function(){
                var data =  $('#wf_form_' + self.form);
                var data1 = data.serializeArray();

                var formData = new FormData();

                for (var i = 0; i <= data1.length - 1; i++) {
                    formData.append(data1[i].name, data1[i].value);
                }

                if (data.find('#new_esser_text_file').length > 0)
                    formData.append('new_esser[more_fields][file]', $('#new_esser_text_file')[0].files[0]);

                $.ajax({
                    type: 'post',
                    async : true,
                    url: (self.dev ? '/app_dev.php' : '') + JSConfig.api.url + 'submit_form/' + self.form,
                    timeout: 60000,
                    processData: false,
                    contentType : false,
                    crossDomain: true,
                    data: formData,
                    dataType: 'json',
                    xhrFields: {
                        withCredentials: true
                    },
                    success: function(msg) {
                        if (msg.result == 'OK') {
                            if(self.callback)
                                self.callback();
                            if(self.redirect_url)
                                window.location.href = self.redirect_url;
                        }
                        self.container.html(msg.template);
                        self.Init.Form();
                    }
                });
            },
            Proxy : function(){
                self.Set.QueryField();
                $('#wf_form_' + self.form).ajaxSubmit({
                    url: self.proxy + '?form=' + self.form,
                    type: 'POST',
                    success: function(msg) {
                        msg = JSON.parse(msg);
                        if (msg.result == 'OK') {
                            if(self.callback)
                                self.callback();
                            if(self.redirect_url)
                                window.location.href = self.redirect_url;
                        }
                        self.container.html(msg.template);
                        self.Init.Form();
                    }
                });
            }
        }
    };
    self.Set = {
        Container: function() {
            if(!self.container)
                self.container = $('#wf_form_' + self.form).parent();
            else
                self.container = $('#' + self.container);
            
        },
        CustomJS: function(){
            JSConfig.js.push(self.form);
        },
        CustomCSS: function(){
            JSConfig.css.push(self.form);
        },
        QueryField: function(){
            var url = (self.dev ? '/app_dev.php' : '') + JSConfig.api.url + 'submit_form/' + self.form;
            var queryField = $('#wf_form_' + self.form).find('#' + self.form + '_query');
            if(!queryField.length){
                $('#wf_form_' + self.form).append(
                     '<input type="hidden" name="' + self.form + '[query]" id="' + self.form + '_query" value="' + url + '"/>' 
                );
            }
        }
    };
};