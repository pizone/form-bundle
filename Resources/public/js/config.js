
function StateConfigPZForm($stateProvider, $ocLazyLoadProvider, IdleProvider) {

    // Configure Idle settings
    IdleProvider.idle(5); // in seconds
    IdleProvider.timeout(120); // in seconds

    $ocLazyLoadProvider.config({
        // Set to true if you want to see what and when is dynamically loaded
        debug: false
    });

    $stateProvider
        .state('form', {
            abstract: true,
            url: "",
            templateUrl:  "/bundles/pizoneadmin/tmpl/common/_admin_content.html"
        })
        .state('form.field_new', GetStateConfig('field', 'new'))
        .state('form.field_edit', GetStateConfig('field', 'edit'))
        .state('form.field_list', GetStateConfig('field', 'list'))
        .state('form.status_new', GetStateConfig('status', 'new'))
        .state('form.status_edit', GetStateConfig('status', 'edit'))
        .state('form.status_list', GetStateConfig('status', 'list'))
        .state('form.event_new', GetStateConfig('event', 'new'))
        .state('form.event_edit', GetStateConfig('event', 'edit'))
        .state('form.event_list', GetStateConfig('event', 'list'))
        .state('form.form_new', GetStateConfig('form', 'new'))
        .state('form.form_edit', GetStateConfig('form', 'edit'))
        .state('form.form_list', GetStateConfig('form', 'list'))
        .state('form.message_list', {
            url: "/message/:pageId?sort&order_by&perPage&form_id",
            templateUrl: "/bundles/pizoneform/tmpl/message/index.html",
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load(filesForList);
                }
            }
        })
    ;

    var collections = new LazyLoadCollection();

    function GetStateConfig(name, action){
        var url = '',
            tmpl = '',
            path = "/bundles/pizoneform/tmpl/";
        if(action == 'list'){
            url = "/" + name + "/:pageId?sort&order_by&perPage";
            tmpl = path + name + '/index.html';
        }
        else if (action == 'new'){
            url = "/" + name + "/new";
            tmpl = path + name + '/new.html';
        }
        else if(action == 'edit'){
            url = "/" + name + "/edit/:id";
            tmpl = path + name + '/edit.html';
        }
        return {
            url: url,
                templateUrl: tmpl,
                resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    if(action == 'list')
                        return $ocLazyLoad.load(collections.GetList());
                    else
                        return $ocLazyLoad.load(collections.GetEdit());
                }
            }
        };
    }
}
StateConfigPZForm.$inject = ['$stateProvider', '$ocLazyLoadProvider', 'IdleProvider'];

angular
    .module('PiZone.Form')
    .config(StateConfigPZForm);
