var ruFormFieldPZTranslates = {
    LIST: 'Типы полей',
    NEW: 'Новое поле',
    EDIT: 'Редактировать поле - "{{param}}"',
    FIELD: {
        TITLE: 'Название',
        ALIAS: 'Псевдоним',
        IS_ACTIVE: {
            LABEL: 'Активен?',
            NO: 'Нет',
            YES: 'Да',
            ANY: 'Любой'
        },
        ALL: 'Общий поиск'
    },
    MESSAGE: {
        REQUIRED: {
            TITLE: 'Введите название страницы.',
            ALIAS: 'Введите псевдоним страницы.'
        }
    }
};