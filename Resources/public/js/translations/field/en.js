var enFormFieldPZTranslates = {
    LIST: 'Fields',
    NEW: 'New field',
    EDIT: 'Edit field - "{{param}}"',
    FIELD: {
        TITLE: 'Title',
        ALIAS: 'Alias',
        IS_ACTIVE: {
            LABEL: 'Is active?',
            NO: 'No',
            YES: 'Yes',
            ANY: 'Any'
        },
        ALL: 'Common search'
    },
    MESSAGE: {
        REQUIRED: {
            TITLE: 'Please enter title.',
            ALIAS: 'Please enter alias.'
        }
    }
};