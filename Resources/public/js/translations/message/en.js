var enFormMessPZTranslates = {
    LIST: 'Messages',
    FIELD: {
        IS_ACTIVE: {
            LABEL: 'Is active?',
            NO: 'No',
            YES: 'Yes',
            ANY: 'Any'
        },
        ALL: 'Common search'
    }
};