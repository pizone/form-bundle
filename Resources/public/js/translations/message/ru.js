var ruFormMessPZTranslates = {
    LIST: 'Сообщения',
    FIELD: {
        FORM: 'Форма',
        MESS: 'Сообщение',
        IS_ACTIVE: {
            LABEL: 'Активен?',
            NO: 'Нет',
            YES: 'Да',
            ANY: 'Любой'
        },
        ALL: 'Общий поиск'
    }
};