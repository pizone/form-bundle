var enFormEventPZTranslates = {
    LIST: 'Events',
    NEW: 'New event',
    EDIT: 'Edit event - "{{param}}"',
    TAB: {
        GENERAL: {
            TITLE: 'Main'
        },
        CONTENT: {
            TITLE: 'Default message'
        }
    },
    FIELD: {
        TITLE: 'Title',
        ALIAS: 'Alias',
        DESCRIPTION: 'Description',
        SUBJECT: 'Subject',
        CONTENT: 'Content',
        IS_ACTIVE: {
            LABEL: 'Is active?',
            NO: 'No',
            YES: 'Yes',
            ANY: 'Any'
        },
        ALL: 'Common search'
    },
    MESSAGE: {
        REQUIRED: {
            TITLE: 'Please enter title.',
            ALIAS: 'Please enter alias.',
            SUBJECT: 'Please enter subject.',
            CONTENT: 'Please enter content.'
        }
    }
};