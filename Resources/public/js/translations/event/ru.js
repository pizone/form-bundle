var ruFormEventPZTranslates = {
    LIST: 'Типы событий',
    NEW: 'Новое событие',
    EDIT: 'Редактировать событие - "{{param}}"',
    TAB: {
        GENERAL: {
            TITLE: 'Основные данные'
        },
        CONTENT: {
            TITLE: 'Сообщение по умолчанию'
        }
    },
    FIELD: {
        TITLE: 'Название',
        ALIAS: 'Псевдоним',
        DESCRIPTION: 'Описание',
        SUBJECT: 'Тема',
        CONTENT: 'Контент',
        IS_ACTIVE: {
            LABEL: 'Активен?',
            NO: 'Нет',
            YES: 'Да',
            ANY: 'Любой'
        },
        ALL: 'Общий поиск'
    },
    MESSAGE: {
        REQUIRED: {
            TITLE: 'Введите название.',
            ALIAS: 'Введите псевдоним.',
            SUBJECT: 'Введите тему.',
            CONTENT: 'Введите контент.'
        }
    }
};