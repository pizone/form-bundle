var ruFormStatusPZTranslates = {
    LIST: 'Типы статусов',
    NEW: 'Новый статус',
    EDIT: 'Редактировать статус - "{{param}}"',
    FIELD: {
        TITLE: 'Название',
        ALIAS: 'Псевдоним',
        SORT: 'Сортировка',
        IS_ACTIVE: {
            LABEL: 'Активен?',
            NO: 'Нет',
            YES: 'Да',
            ANY: 'Любой'
        },
        ALL: 'Общий поиск'
    },
    MESSAGE: {
        REQUIRED: {
            TITLE: 'Введите название статуса.',
            ALIAS: 'Введите псевдоним сатуса.'
        }
    }
};