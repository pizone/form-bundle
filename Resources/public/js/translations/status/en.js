var enFormStatusPZTranslates = {
    LIST: 'List status',
    NEW: 'New status',
    EDIT: 'Edit status - "{{param}}"',
    FIELD: {
        TITLE: 'Title',
        ALIAS: 'Alias',
        SORT: 'Sort',
        IS_ACTIVE: {
            LABEL: 'Is active?',
            NO: 'No',
            YES: 'Yes',
            ANY: 'Any'
        },
        ALL: 'Common search'
    },
    MESSAGE: {
        REQUIRED: {
            TITLE: 'Please enter status name.',
            ALIAS: 'Please enter alias.'
        }
    }
};