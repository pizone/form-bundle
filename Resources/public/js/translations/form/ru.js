var ruFormPZTranslates = {
    LIST: 'Формы',
    NEW: 'Новая форма',
    EDIT: 'Редактировать форму - "{{param}}"',
    EDIT_FIELD: 'Редактировать поле',
    NEW_FIELD: 'Новое поле',
    ADD_FIELD: 'Добавить поле',
    EDIT_EVENT: 'Редактировать событие',
    ADD_TMPL: 'Добавить шаблон',
    EDIT_TMPL: 'Редактировать шаблон',
    DEFAULT_TMPL: 'Шаблон по умолчанию',
    NOT_EXIST_FIELDS: 'В форму не добавлено ни одного поля',
    TAB: {
        GENERAL: {
            TITLE: 'Основные данные'
        },
        FIELDS: {
            TITLE: 'Поля формы'
        },
        EVENTS: {
            TITLE: 'События'
        },
        MAIL_TO: {
            TITLE: 'Адреса администраторов'
        },
        MAIN: {
            TITLE: 'Основное'
        },
        CONTENT: {
            TITLE: 'Сообщение по умолчанию'
        },
        ATTR: {
            TITLE: 'Атрибуты'
        },
        VALIDATION: {
            TITLE: 'Валидация'
        },
        MORE: {
            TITLE: 'Дополнительно'
        },
        TMPL:{
            TITLE: 'Шаблоны'
        },
        CSS:{
            TITLE: 'CSS'
        },
        JS:{
            TITLE: 'Javascript'
        }
    },
    FIELD: {
        TITLE: 'Название',
        ALIAS: 'Псевдоним',
        DESCRIPTION: 'Описание',
        DOMAINS: 'Домены',
        CLASS: 'Class',
        PLACEHOLDER: 'Placeholder',
        ATTR: 'Аттрибуты',
        IS_REQUIRED: 'Обязательное?',
        REGEXP: 'Регулярное выражение',
        DEFAULT: 'Значение по умолчанию',
        FIELD: 'Тип поля',
        SORT: 'Сортировка',
        MAIL_TO: 'Список адресов администраторов',
        EVENTS: 'События',
        SUBJECT: 'Тема письма',
        CONTENT: 'Контент',
        TMPL: 'Шаблон',
        CSS: 'CSS',
        JS: 'Javascript',
        IS_ACTIVE: {
            LABEL: 'Активен?',
            NO: 'Нет',
            YES: 'Да',
            ANY: 'Любой'
        },
        ALL: 'Общий поиск'
    },
    MESSAGE: {
        REQUIRED: {
            TITLE: 'Введите название.',
            ALIAS: 'Введите псевдоним.',
            SUBJECT: 'Введите тему.',
            CONTENT: 'Введите контент.'
        }
    }
};