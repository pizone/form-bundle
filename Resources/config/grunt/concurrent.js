module.exports = {
    // Опции
    options: {
        limit: 3
    },

    // Задачи разработки
    devFirstForm: [
        'jshint'
    ],
    devSecondForm: [
        'uglify'
    ],


    // Производственные задачи
    prodFirstForm: [
        'jshint'
    ],
    prodSecondForm: [
        'uglify'
    ]
};