module.exports = {
    my_target: {
        files: {
            'web/assetic/js/form.min.js': [
                'web/bundles/pizoneform/js/app.js',
                'web/bundles/pizoneform/js/translations/**/*.js',
                'web/bundles/pizoneform/js/translations.js',
                // 'web/bundles/pizoneform/js/config.js',
                'web/bundles/pizoneform/js/controller/**/*.js',
                'web/bundles/pizoneform/js/model/**/*.js'
            ],
            'web/assetic/js/wf.form.min.js': [
                'web/bundles/pizoneform/js/client/jquery.form.min.js',
                'web/bundles/pizoneform/js/client/jquery.maskedinput.min.js',
                'web/bundles/pizoneform/js/client/jquery.placeholder.min.js',
                'web/bundles/pizoneform/js/client/wf.form.js'
            ]
        }
    }
};