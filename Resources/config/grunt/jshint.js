module.exports = {
    options: {
        reporter: require('jshint-stylish')
    },
    main: [
        'web/bundles/pizoneform/js/**/*.js',
        '!web/bundles/pizoneform/js/client/jquery.form.min.js',
        '!web/bundles/pizoneform/js/client/jquery.maskedinput.min.js',
        '!web/bundles/pizoneform/js/client/jquery.placeholder.js'
    ]
};