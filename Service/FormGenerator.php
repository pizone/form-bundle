<?php

namespace PiZone\FormBundle\Service;

use Symfony\Component\HttpFoundation\JsonResponse;
use DOMDocument;
use DOMAttr;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Response;

class FormGenerator {

    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * Constructor
     * 
     * @param ContainerInterface $container
     */
    public function __construct($container) {
        $this->container = $container;
    }

    public function getMoreFields($form, $valueForm, $error) {
        $moreField = array();
        $fields = $form->getFormFields();
        $value = $valueForm->getMoreField();

        foreach ($fields as $one) {
            if ($one->isActive()) {
                $moreField[$one->getAlias()] = array(
                    'label' => $one->getField()->getAlias() != 'hidden' ? $this->getLabel($one): "",
                    'field' => $this->renderField($one, $value, isset($error[$one->getAlias()]) ? $error[$one->getAlias()] : null),
                    'error' => isset($error[$one->getAlias()]) ? $error[$one->getAlias()] : null);
            } else {
                $moreField[$one->getAlias()] = array(
                    'label' => '',
                    'field' => '',
                    'error' => '');
            }
        }

        return $moreField;
    }

    public function renderField($field, $valueMoreFields, $error) {
        $str = "";
        if ($field->isActive()) {
            if (isset($valueMoreFields[$field->getAlias()]))
                $valueField = htmlspecialchars($this->getValueField($valueMoreFields[$field->getAlias()]));
            else {
                if (!$error)
                    $valueField = htmlspecialchars($field->getDefaultValue());
                else
                    $valueField = '';
            }

            if ($field->getField()->getAlias() == 'text')
                $str = $this->renderTextField($field, $valueField);

            if ($field->getField()->getAlias() == 'email')
                $str = $this->renderTextField($field, $valueField);

            if ($field->getField()->getAlias() == 'integer')
                $str = $this->renderTextField($field, $valueField);
            
            if ($field->getField()->getAlias() == 'hidden')
                $str = $this->renderHiddenField($field, $valueField);

            if ($field->getField()->getAlias() == 'textarea')
                $str = $this->renderTextareaField($field, $valueField);

            if ($field->getField()->getAlias() == 'file')
                $str = $this->renderFileField($field, $valueField);
        }

        return $str;
    }

    private function renderTextField($field, $valueField) {
        $str = '<input type="' . $field->getField()->getAlias() . '" name="' . $field->getForm()->getAlias() . '[more_fields][' . $field->getAlias() . ']" value="' . $valueField . '" id="' . $field->getForm()->getAlias() . '_text_' . $field->getAlias() . '"';
        if ($field->getPlaceholder())
            $str .= ' placeholder="' . $field->getPlaceholder() . '"';
        if ($field->getAttr())
            $str .= ' ' . $field->getAttr();
        if ($field->getClass())
            $str .= ' class="' . $field->getClass() . '"';
        $str .= '/>';

        return $str;
    }
    
    private function renderHiddenField($field, $valueField) {
        $str = '<input type="hidden" name="' . $field->getForm()->getAlias() . '[more_fields][' . $field->getAlias() . ']" value="' . $valueField . '" id="' . $field->getForm()->getAlias() . '_text_' . $field->getAlias() . '"';
        if ($field->getAttr())
            $str .= ' ' . $field->getAttr();
        if ($field->getClass())
            $str .= ' class="' . $field->getClass() . '"';
        $str .= '/>';

        return $str;
    }

    private function renderTextareaField($field, $valueField) {
        $str = '<textarea name="' . $field->getForm()->getAlias() . '[more_fields][' . $field->getAlias() . ']" id="' . $field->getForm()->getAlias() . '_textarea_' . $field->getAlias() . '"';
        if ($field->getPlaceholder())
            $str .= ' placeholder="' . $field->getPlaceholder() . '"';
        if ($field->getAttr())
            $str .= ' ' . $field->getAttr();
        if ($field->getClass())
            $str .= ' class="' . $field->getClass() . '"';
        $str .= '>' . $valueField . '</textarea>';

        return $str;
    }

    private function renderFileField($field, $valueField) {
        $str = '<input type="file" name="' . $field->getForm()->getAlias() . '[more_fields][' . $field->getAlias() . ']" id="' . $field->getForm()->getAlias() . '_text_' . $field->getAlias() . '" value=""';
        if ($field->getAttr())
            $str .= ' ' . $field->getAttr();
        if ($field->getClass())
            $str .= ' class="' . $field->getClass() . '"';
        $str .= '/>';

        return $str;
    }

    public function getValueField($value) {
        if ($value)
            return $value;
        return null;
    }

    public function getLabel($field) {
        return '<label for="' . $field->getForm()->getAlias() . '_' . $field->getAlias() . '">' . $field->getTitle() . '</label>';
    }

    public function renderTemplate($form, $valueForm = null, $error = null) {
        $moreFields = $this->getMoreFields($form, $valueForm, $error);

        $template = $this->getTemplate($form);
        
        $regexp = "/{{more_fields_block}}(.*){{end_more_fields_block}}/isU";
        $pattern = preg_match($regexp, $template, $matches, PREG_OFFSET_CAPTURE);

        if ($pattern !== false && $pattern !== 0) {
            $block = "";
            foreach ($moreFields as $key => $one) {
                $blockPattern = $matches[1][0];
                $blockPattern = str_replace('{{field_label}}', $one['label'], $blockPattern);
                $blockPattern = str_replace('{{field}}', $one['field'], $blockPattern);
                $blockPattern = str_replace('{{field_error}}', $this->renderError($one['error']), $blockPattern);
                $block .= $blockPattern;
            }

            $template = preg_replace($regexp, $block, $template);
        }

        foreach ($moreFields as $key => $one) {
            $template = str_replace('{{' . $key . '_label}}', $one['label'], $template);
            $template = str_replace('{{' . $key . '}}', $one['field'], $template);
            $template = str_replace('{{' . $key . '_error}}', $this->renderError($one['error']), $template);
        }

        $template = $this->normalizeTemplate($template, $form);

        return $template;
    }
    
    public function compileTemplate($form, $moreFields){
        $template = $this->getTemplate($form);
        
        $regexp = "/{{more_fields_block}}(.*){{end_more_fields_block}}/isU";
        $pattern = preg_match($regexp, $template, $matches, PREG_OFFSET_CAPTURE);

        if ($pattern !== false && $pattern !== 0) {
            $block = "";
            foreach ($moreFields as $one) {
                $blockPattern = $matches[1][0];
                $blockPattern = str_replace('{{field_label}}', '{{'.$one.'_label}}', $blockPattern);
                $blockPattern = str_replace('{{field}}', '{{'.$one.'}}', $blockPattern);
                $blockPattern = str_replace('{{field_error}}', '{{'.$one.'_error}}', $blockPattern);
                $block .= trim($blockPattern);
            }

            $template = preg_replace($regexp, $block, $template);
        }
        
        return $template;
    }

    private function getTemplate($form) {
        $template = "";
        if ($form->getTemplate() && $form->getTemplate()->isActive())
            $template = $form->getTemplate()->getTemplate();

        if (!trim($template)) {
            $objTemplete = $this->get('pi_zone.form_template')->findTemplate();
            if ($objTemplete)
                $template = $objTemplete->getTemplate();
        }

        return $template;
    }

    private function getDoctrine() {
        return $this->container->get('doctrine');
    }

    public function renderError($error) {
        $str = null;
        if ($error) {
            $str = '<ul class="error">';
            foreach ($error as $i => $one) {
                $str .= '<li>' . $one . '</li>';
            }
            $str .= '</ul>';
        }
        return $str;
    }

    public function normalizeTemplate($template, $form) {
        $dom = new DOMDocument();
        $dom->loadHTML('<?xml encoding="utf-8" ?>' . $template);
        $teg = $dom->getElementsByTagName('form');

        $action = new DOMAttr('action', '/api/client/form_message/submit_form/' . $form->getAlias());
        $teg->item(0)->appendChild($action);
        $action = new DOMAttr('method', 'post');
        $teg->item(0)->appendChild($action);
        $action = new DOMAttr('name', $form->getAlias());
        $teg->item(0)->appendChild($action);
        $id = new DOMAttr('id', 'wf_form_' . $form->getAlias());
        $teg->item(0)->appendChild($id);

        $template = str_replace('<?xml encoding="utf-8" ?>', '', $dom->saveHTML());

        return $template;
    }

    public function formValidate($form) {
        $error = array();
        $moreField = $form->getMoreField();
        foreach ($form->getForm()->getFormFields() as $field) {
            if ($field->isActive()) {
                if ($field->isRequired() && !trim(isset($moreField[$field->getAlias()]) ? $moreField[$field->getAlias()] : ""))
                    $error[$field->getAlias()][] = $this->DefaultErrorMessage();
                elseif ($field->getRegexpPattern() && !preg_match($field->getRegexpPattern(), isset($moreField[$field->getAlias()]) ? $moreField[$field->getAlias()] : ""))
                    $error[$field->getAlias()][] = 'Неверный формат';
            }
        }

        return $error;
    }

    public function DefaultErrorMessage() {
        return 'Поле должно быть заполнено.';
    }

}
