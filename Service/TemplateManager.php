<?php

namespace PiZone\FormBundle\Service;

class TemplateManager
{
    /**
     * @var ContainerInterface
     */
    protected $container;
    protected $doctrine;

    /**
     * Constructor
     *
     * @param ContainerInterface $container
     */
    public function __construct($container)
    {
        $this->container = $container;
        $this->doctrine = $container->get('doctrine')->getManager('default');
    }

    public function findTemplate($filename)
    {
        return $this->doctrine->getRepository('PiZoneFormBundle:Template')->findTemplate($filename);
    }
}