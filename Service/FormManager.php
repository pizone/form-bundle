<?php

namespace PiZone\FormBundle\Service;

class FormManager
{
    /**
     * @var ContainerInterface
     */
    protected $container;
    protected $doctrine;

    /**
     * Constructor
     *
     * @param ContainerInterface $container
     */
    public function __construct($container)
    {
        $this->container = $container;
        $this->doctrine = $container->get('doctrine')->getManager('default');
    }

    public function find($id){
        return $this->doctrine->getRepository('PiZoneFormBundle:Form')->find($id);
    }

    public function findFormByAlias($filename)
    {
        return $this->doctrine->getRepository('PiZoneFormBundle:Form')->findFormByAlias($filename);
    }
}