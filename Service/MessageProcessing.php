<?php
namespace PiZone\FormBundle\Service;

class MessageProcessing{
    /**
     * @var ContainerInterface
     */
    protected $container;
    
    protected $attachment;

    /**
     * Constructor
     * 
     * @param ContainerInterface $container
     */
    public function __construct($container) {
        $this->container = $container;
        $this->attachment = array();
    }
    
    public function adaptationContent($content, $value){
        $formFields = $this->container->get('doctrine')->getManager('default')->getRepository('PiZoneFormBundle:FormField')->findBy(array('form' => $value->getForm()));
        
        $content = str_replace('{{form}}', $value->getForm()->getTitle(), $content);
        $content = str_replace('{{created_at}}', $value->getCreatedAt()->format('d.m.Y H:i:s'), $content);
        
        $more = $value->getMoreField();
        foreach($formFields as $one){
            if(isset($more[$one->getAlias()]) && $more[$one->getAlias()]){
                switch ($one->getField()->getAlias()){
                    case 'file':
                        $file = $more[$one->getAlias()];

                        if(isset($file['path'])) {

                            $this->attachment[] = array('path' => $value->getAbsolutePath($file['path']), 'origin_name' => $file['origin_name']);
                        }
                        break;
                    default :
                        $content = str_replace('{{'.$one->getAlias().'}}', $more[$one->getAlias()], $content);
                }
            }
        }
        
        return $content;
    }
    
    public function getMailTo($form){
        $result = array();
        $mailToArray = explode("\n", $form);
        foreach($mailToArray as $one){
            $one = trim(str_replace(array("\r\n", "\r", "\n"), '', $one));
            $result[] = $one;
        }
        
        return $result;
    }
    
    public function writeToFile($filename, $text) {
        if (!$filename)
            return false;
        for ($i = 0; $i < 1000; $i++) {
            $f = fopen($filename, 'a');

            if (flock($f, LOCK_EX)) {
                fwrite($f, $text . "\r\n");
                flock($f, LOCK_UN);
                fclose($f);
                break;
            } else {
                fclose($f);
                usleep(50);
            }
        }

        return true;
    }
    
    public function randomStr($length) {
        $abc = 'abcdefghijklmnopqrstuvwxyz-1234567890_ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $rand_max = strlen($abc) - 1;
        $str = '';
        for ($i = 0; $i < $length; $i++) {
            $rand = rand(0, $rand_max);
            $letter = $abc{$rand};
            $str .= $letter;
        }
        return($str);
    }
    
    public function eventControl($value){
        foreach($value->getForm()->getEvents() as $event){
            if($event->getAlias() == 'message_to_user') {
                $this->sendMessageForUser($value, $event);
            }
            if($event->getAlias() == 'message_to_admin')
                $this->sendMessageForAdmin ($value, $event);
        }
    }
    
    public function sendMessageForUser($value, $event){
        $fields = $value->getMoreField();
        if(isset($fields['email'])) {
            $mailer = $this->container->get('mailer');

            $eventMesage = $this->getMessage($value, $event);

            $message = \Swift_Message::newInstance()
                ->setSubject($eventMesage['subject'])
                ->setContentType('text/html')
                ->setFrom($this->container->getParameter('mailer_mail_from'))
                ->setTo($fields['email'])
                ->setBody($this->adaptationContent($eventMesage['content'], $value));
            $mailer->send($message);
        }
    }
    
    public function sendMessageForAdmin($value, $event){
        $mailer = $this->container->get('mailer');
        
        $eventMesage = $this->getMessage($value, $event);
        
        $mailTo = $this->getMailTo($value->getForm()->getMailTo());
        
        $message = \Swift_Message::newInstance()
           ->setSubject($eventMesage['subject'])
           ->setContentType('text/html')
           ->setFrom($this->container->getParameter('mailer_mail_from'))
           ->setTo($mailTo)
           ->setBody($this->adaptationContent($eventMesage['content'], $value));
        foreach($this->attachment as $one){
            if ($one['path']) {
                $message->attach(\Swift_Attachment::fromPath($one['path'])->setFilename($one['origin_name']));
            }
        }
        $mailer->send($message);
    }
    
    private function getMessage($value, $event){
        $result = array();
        $customEvent = $this->container->get('doctrine')->getManager('default')->getRepository('PiZoneFormBundle:FormEvent')
                ->findOneBy(array(
                    'form' => $value->getForm(),
                    'event' => $event
                ));
        if($customEvent->getSubject())
            $result['subject'] = $customEvent->getSubject();
        else
            $result['subject'] = $event->getSubject();
        
        if($customEvent->getContent())
            $result['content'] = $customEvent->getContent();
        else
            $result['content'] = $event->getContent();
        
        return $result;
    }
}

