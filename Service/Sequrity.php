<?php
namespace PiZone\FormBundle\Service;

class Sequrity{
    
    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * Constructor
     * 
     * @param ContainerInterface $container
     */
    public function __construct($container) {
        $this->container = $container;
    }
    
    public function check(){
        $form = $this->getForm();  
        if($form){
            $domains = $this->getFormDomains($form);
            if(count($domains) == 0)
                return true;
            
            $guest = $this->getDomainReferer();
            
            foreach($domains as $one){
                if($one == $guest)
                    return true;
            }
        }
        
        return false;
    }
    
    private function getDomainReferer(){
        $domain = '';
        $referer = $this->getRequest()->server->get('HTTP_REFERER');
        if($referer){
            $parse = parse_url($referer);
            $domain = $parse['host'];
        }
        return $domain;
    }
    
    private function getRequest()
    {
        return $this->container->get('request_stack')->getCurrentRequest();
    }
    
    private function getForm(){
        $aliasForm = $this->getRequest()->get('form');
        $form = $this->container->get('pi_zone.form')->findFormByAlias($aliasForm);

        return $form;
    }
    
    private function getFormDomains($form){
        $domains = array();
        if($form->getDomains()){
            $domains = explode("\n", $form->getDomains());
            $domains = array_diff($domains, array(''));
        }
        
        return $domains;
    }
}

