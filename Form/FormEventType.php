<?php

namespace PiZone\FormBundle\Form;

use PiZone\AdminBundle\Form\Type\EntityHiddenType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FormEventType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('event', EntityHiddenType::class, array(
                'class' => 'PiZone\FormBundle\Entity\Event',
                'em' => 'default'
            ))
            ->add('subject', TextType::class, array(
                'label' => 'FFORM.FIELD.SUBJECT',
                'required' => false
            ))
            ->add('content', TextareaType::class, array(
                'label' => 'FFORM.FIELD.CONTENT',
                'required' => false
            ))
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'validation_groups' => array('PZFormEvent'),
            'data_class' => 'PiZone\FormBundle\Entity\FormEvent'
        ));
    }
}
