<?php

namespace PiZone\FormBundle\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EventType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('alias', TextType::class, array(
                'label' => 'FEVENT.FIELD.ALIAS'
            ))
            ->add('title', TextType::class, array(
                'label' => 'FEVENT.FIELD.TITLE'
            ))
            ->add('description', TextareaType::class, array(
                'label' => 'FEVENT.FIELD.DESCRIPTION'
            ))
            ->add('subject', TextType::class, array(
                'label' => 'FEVENT.FIELD.SUBJECT'
            ))
            ->add('content', TextareaType::class, array(
                'label' => 'FEVENT.FIELD.CONTENT'
            ))
            ->add('is_active', CheckboxType::class, array(
                'label' => 'FEVENT.FIELD.IS_ACTIVE.LABEL'
            ))
        ;

    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'validation_groups' => array('PZEvent'),
            'data_class' => 'PiZone\FormBundle\Entity\Event'
        ));
    }
}
