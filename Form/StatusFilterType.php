<?php

namespace PiZone\FormBundle\Form;

use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class StatusFilterType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('all', TextType::class, array(
                'label' => 'FSTATUS.FIELD.ALL',
                'required' => false
            ))
            ->add('alias', TextType::class, array(
                'label' => 'FSTATUS.FIELD.ALIAS',
                'required' => false
            ))
            ->add('title', TextType::class, array(
                'label' => 'FSTATUS.FIELD.TITLE',
                'required' => false
            ))
            ->add('sort', TextType::class, array(
                'label' => 'FSTATUS.FIELD.SORT',
                'required' => false
            ))
            ->add('is_active',  ChoiceType::class, array(
                'choices' =>   array('FSTATUS.FIELD.IS_ACTIVE.NO' => 0,    'FSTATUS.FIELD.IS_ACTIVE.YES' => 1,),
                'label' => 'FSTATUS.FIELD.IS_ACTIVE.LABEL',
                'choices_as_values' => true,
                'placeholder' => 'FSTATUS.FIELD.IS_ACTIVE.ANY',
                'empty_data'  => null,
                'required' => false
            ))

        ;
    }
}
