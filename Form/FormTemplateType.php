<?php

namespace PiZone\FormBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FormTemplateType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, array(
                'label' => 'FFORM.FIELD.TITLE'
            ))
            ->add('template', TextareaType::class, array(
                'label' => 'FFORM.FIELD.TMPL'
            ))
            ->add('css', TextareaType::class, array(
                'label' => 'FFORM.FIELD.CSS'
            ))
            ->add('js', TextareaType::class, array(
                'label' => 'FFORM.FIELD.JS'
            ))
            ->add('is_active', CheckboxType::class, array(
                'label' => 'FFORM.FIELD.IS_ACTIVE.LABEL'
            ))
            ->add('is_selected', HiddenType::class, array(
                'label' => 'FFORM.FIELD.IS_SELECTED'
            ))
        ;

    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'validation_groups' => array('PZTemplate'),
            'data_class' => 'PiZone\FormBundle\Entity\FormTemplate'
        ));
    }
}
