<?php

namespace PiZone\FormBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class FormValueFilterType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('all', TextType::class, array(
                'label' => 'FMESS.FIELD.ALL',
                'required' => false
            ))
            ->add('form', EntityType::class, array(
                'label' => 'FMESS.FIELD.FORM',
                'class' => 'PiZoneFormBundle:Form',
                'required' => false
            ))
            ->add('form_field', TextType::class, array(
                'label' => 'FMESS.FIELD.MESS',
                'required' => false
            ))
        ;
    }
}
