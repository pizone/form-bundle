<?php

namespace PiZone\FormBundle\Form;


use PiZone\AdminBundle\Form\Type\EntityHiddenType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FormType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, array(
                'label' => 'FFORM.FIELD.TITLE'
            ))
            ->add('alias', TextType::class, array(
                'label' => 'FFORM.FIELD.ALIAS'
            ))
            ->add('description', TextareaType::class, array(
                'label' => 'FFORM.FIELD.DESCRIPTION'
            ))
            ->add('domains', TextareaType::class, array(
                'label' => 'FFORM.FIELD.DOMAINS'
            ))
            ->add('mail_to', TextareaType::class, array(
                'label' => 'FFORM.FIELD.MAIL_TO'
            ))
            ->add('events', EntityType::class, array(
                'required' => true,
                'multiple' => true,
                'class' => 'PiZoneFormBundle:Event',
                'expanded' => true
            ))
            ->add('template', EntityHiddenType::class, array(
                'class' => 'PiZoneFormBundle:FormTemplate',
                'em' => 'default'
            ))
            ->add('form_templates', CollectionType::class, array(
                'entry_type' => FormTemplateType::class,
                'label' => 'FFORM.FIELD.FORM_TEMPLATES',
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false
            ))
            ->add('form_fields', CollectionType::class, array(
                'entry_type' => FormFieldType::class,
                'label' => 'FFORM.FIELD.FORM_FIELDS',
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false
            ))
            ->add('form_event', CollectionType::class, array(
                'entry_type' => FormEventType::class,
                'label' => 'FFORM.FIELD.FORM_EVENT',
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false
            ))
            ->add('is_active', CheckboxType::class, array(
                'label' => 'FFORM.FIELD.IS_ACTIVE.LABEL'
            ));
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'validation_groups' => array('PZForm'),
            'data_class' => 'PiZone\FormBundle\Entity\Form'
        ));
    }
}
