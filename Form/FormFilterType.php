<?php

namespace PiZone\FormBundle\Form;

use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class FormFilterType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('all', TextType::class, array(
                'label' => 'FFORM.FIELD.ALL',
                'required' => false
            ))
            ->add('alias', TextType::class, array(
                'label' => 'FFORM.FIELD.ALIAS',
                'required' => false
            ))
            ->add('title', TextType::class, array(
                'label' => 'FFORM.FIELD.TITLE',
                'required' => false
            ))
            ->add('description', TextType::class, array(
                'label' => 'FFORM.FIELD.DESCRIPTION',
                'required' => false
            ))
            ->add('domains', TextType::class, array(
                'label' => 'FFORM.FIELD.DOMAINS',
                'required' => false
            ))
            ->add('mail_to', TextType::class, array(
                'label' => 'FFORM.FIELD.MAIL_TO',
                'required' => false
            ))
            ->add('is_active',  ChoiceType::class, array(
                'choices' =>   array('FFORM.FIELD.IS_ACTIVE.NO' => 0,    'FFORM.FIELD.IS_ACTIVE.YES' => 1,),
                'label' => 'FFORM.FIELD.IS_ACTIVE.LABEL',
                'choices_as_values' => true,
                'placeholder' => 'FFORM.FIELD.IS_ACTIVE.ANY',
                'empty_data'  => null,
                'required' => false
            ))

        ;
    }
}
