<?php

namespace PiZone\FormBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FormFieldType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, array(
                'label' => 'FFORM.FIELD.TITLE'
            ))
            ->add('alias', TextType::class, array(
                'label' => 'FFORM.FIELD.ALIAS'
            ))
            ->add('class', TextType::class, array(
                'label' => 'FFORM.FIELD.CLASS'
            ))
            ->add('placeholder', TextType::class, array(
                'label' => 'FFORM.FIELD.PLACEHOLDER'
            ))
            ->add('attr', TextType::class, array(
                'label' => 'FFORM.FIELD.ATTR'
            ))
            ->add('is_required', CheckboxType::class, array(
                'label' => 'FFORM.FIELD.IS_REQUIRED'
            ))
            ->add('sort', TextType::class, array(
                'label' => 'FFORM.FIELD.SORT'
            ))
            ->add('default_value', TextType::class, array(
                'label' => 'FFORM.FIELD.DEFAULT'
            ))
            ->add('is_active', CheckboxType::class, array(
                'label' => 'FFORM.FIELD.IS_ACTIVE.LABEL'
            ))
            ->add('regexp_pattern', TextType::class, array(
                'label' => 'FFORM.FIELD.REGEXP'
            ))
            ->add('field', EntityType::class, array(
                'label' => 'FFORM.FIELD.FIELD',
                'class' => 'PiZoneFormBundle:Field'
            ))
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'validation_groups' => array('PZFormField'),
            'data_class' => 'PiZone\FormBundle\Entity\FormField'
        ));
    }
}
