<?php

namespace PiZone\FormBundle\Controller\Message;

use PiZone\AdminBundle\Controller\AActionController;

/**
 * WebItem controller.
 *
 */
class ActionController extends AActionController
{
    public function __construct(){
        $this->model = 'PiZone\FormBundle\Entity\FormValue';
        $this->repository = 'PiZone\FormBundle\Entity\FormValue';
        $this->route['delete'] = 'form_message_delete';
        $this->route['list']['name'] = 'form_message';
    }
}