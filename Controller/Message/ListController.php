<?php

namespace PiZone\FormBundle\Controller\Message;

use Doctrine\Common\Collections\ArrayCollection;
use PiZone\AdminBundle\Controller\AListController;
use PiZone\AdminBundle\Controller\IListController;
use Symfony\Component\HttpFoundation\Request;

/**
 * User list controller.
 *
 */
class ListController  extends AListController implements IListController
{
    protected $userId = null;

    public function __construct(){
        $this->prefixSession = 'PiZone\FormBundle\FormMessageList';
        $this->routeList = array(
            'list' => array(
                'name' => 'form_message',
                'parameters' => array()
            ),
            'delete' => 'form_message_delete',
            'batch' => array(
                'delete' => 'form_message_batch_delete',
                'active' => 'form_message_batch_active'
            )
        );
        $this->model = 'PiZone\FormBundle\Entity\FormValue';
        $this->repository = 'PiZone\FormBundle\Entity\FormValue';
        $this->filtersForm = 'PiZone\FormBundle\Form\FormValueFilterType';
    }

    protected function parseRequest(Request $request){
        if($request->get('formId')){
            $form = $this->get('pi_zone.form')->find($request->get('formId'));
            $this->get('session')->set($this->prefixSession . '\Filters', array('form' => $form));
        }

    }

    protected function getlist($entities){
        $tokenManager = $this->get('security.csrf.token_manager');

        foreach ($entities as $one) {
            $tokenId = $this->generateUrl($this->routeList['delete'], array('id' => $one->getId()));
            $token = $tokenManager->getToken($tokenId);

            $resFields = array(
                'id' => $one->getId(),
                'form' => array(
                    'id' => $one->getForm()->getId(),
                    'title' => $one->getForm()->getTitle(),
                    'alias' => $one->getForm()->getAlias()
                ),
                'fields' => array(),
                '_token' => $token->getValue()
            );
            $fields = $one->getMoreField();
            $formFields = $one->getForm()->getFormFields();
            $fieldKey = array();
            foreach($formFields as $fFiled){
                $fieldKey[$fFiled->getAlias()] = $fFiled->getTitle();
            }

            foreach($fields as $key => $field){
                $resFields['fields'][] = array('key' => $key, 'value' => $field, 'title' => $fieldKey[$key]);
            }

            $this->fieldList[] = $resFields;
        }

        return $this->fieldList;
    }

    protected function getFilterForm()
    {
        $filters = $this->getFilters();
        if(isset($filters['form']) && $filters['form']) {
            $em = $this->getDoctrine()->getManager($this->manager);
            $em->persist($filters['form']);
        }

        return $this->createForm($this->getFiltersType(), $filters);
    }


    protected function processFilters($query)
    {
        $filterObject = $this->getFilters();

        $queryFilter = $this->getQueryFilter();
        $queryFilter->setQuery($query);

        if (isset($filterObject['all']) && null !== $filterObject['all']) {
            $queryFilter->addOrStringFilter('more_filed', $filterObject['all']);
        }
        if (isset($filterObject['form']) && null !== $filterObject['form']) {
            $queryFilter->addDefaultFilter('form', $filterObject['form']);
        }
        if (isset($filterObject['more_filed']) && null !== $filterObject['more_filed']) {
            $queryFilter->addOrStringFilter('more_filed', $filterObject['more_filed']);
        }
    }
}