<?php

namespace PiZone\FormBundle\Controller\Message;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use PiZone\FormBundle\Entity\FormValue;
use PiZone\FormBundle\Entity\Form;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use PiZone\FormBundle\Entity\FormStatus;
use Symfony\Component\HttpFoundation\Request;


class AjaxController extends Controller
{   
    protected $formResult;
    public function getTemplateAction(Request $request, $form){
        if(!$this->get('pi_zone.form_sequrity')->check())
            throw $this->createNotFoundException();
        
        $callback = $request->get('callback');
        
        $form = $this->get('pi_zone.form')->findFormByAlias($form);

        $template = null;
        if($form){
            $valueForm = new FormValue();
            $valueForm->setForm($form);
            $template = $this->get('pi_zone.form_generator')->renderTemplate($form, $valueForm);
        }
        
        $response = new JsonResponse(array('template' => $template), 200, array());
        if($callback)
            $response->setCallback($callback);
        return $response;
    }
    
    public function submitFormAction(Request $request, $form){
        if(!$this->get('pi_zone.form_sequrity')->check())
            throw $this->createNotFoundException();
        
        $callback = $request->get('callback');
        
        $em = $this->getDoctrine()->getManager();
        $formObject = $this->get('pi_zone.form')->findFormByAlias($form);
        
        $this->formResult = $request->get($form);

        $files = $this->getFormFiles($form);
        
        $status = $this->getDoctrine()->getManager('default')->getRepository('PiZoneFormBundle:Status')->findOneBy(array('alias' => 'new'));
        
        $formStatus = new FormStatus();
        $formStatus->setStatus($status);
        $valueForm = new FormValue($this->formResult);
        $valueForm->setForm($formObject);
        $valueForm->addFormStatus($formStatus);
        $valueForm->addFiles($files);

        $error = $this->get('pi_zone.form_generator')->formValidate($valueForm);
        
        if(!$error){
            $em->persist($valueForm);
            $em->flush();

            $result = 'OK';
            $this->get('pi_zone.form_processing')->eventControl($valueForm);
            $template = null;
            if($formObject){
                $valueForm = new FormValue();
                $valueForm->setForm($formObject);
                $template = $this->get('pi_zone.form_generator')->renderTemplate($formObject, $valueForm);
            }
        }
        else{
            $result = 'ERROR';
            $template = null;
            if($formObject)
                $template = $this->get('pi_zone.form_generator')->renderTemplate($formObject, $valueForm, $error);
        }

        
        $response = new JsonResponse(array('template' => $template, 'result' => $result), 200, array());
        if($callback)
            $response->setCallback($callback);
        return $response;
    }
    
    public function getJSAction($filename){
//        if(!$this->get('form_sequrity')->check())
//            throw $this->createNotFoundException();
        
        $filePath = $this->get('kernel')->getRootDir().'/../web/assetic/js/'.$filename;

        if (!file_exists($filePath)) {
            $formObject = $this->get('pi_zone.form')->findFormByAlias($filename);
            
            if($formObject){
                if(!$formObject->getTemplate())
                    $template = $this->get('pi_zone.form_template')->findTemplate();
                else
                    $template = $formObject->getTemplate();
                
                if(!$template)
                    throw $this->createNotFoundException();
                
                $file = md5($template->getJs() . date("D M j G:i:s T Y"). $this->get('pi_zone.form_processing')->randomStr(10)).'.js';
                $filePath = $this->getParameter('pi_zone_form.uploads.files.absolute_path') . $file;
                $str = "EventDispatcher.AddEventListener('onexecute.WfScripts_" . $filename . "', function(){"
                        . $template->getJs().
                        "});";
                        
                $this->get('pi_zone.form_processing')->writeToFile($filePath, $str);
            }
            else
                throw $this->createNotFoundException();
        }

        $response = new BinaryFileResponse($filePath);
        $response->headers->set('Content-Type', 'text/javascript');
        $response->headers->set('Content-Disposition', 'attachment;filename="'.$filename);
        $response->headers->set('Content-Disposition', 'attachment;filename="'.$filename);
        
        return  $response;
    }
    
    public function getCSSAction($form){
        
//        if(!$this->get('form_sequrity')->check())
//            throw $this->createNotFoundException();

        $em = $this->getDoctrine()->getManager('default');
        $formObject = $em->getRepository('PiZoneFormBundle:Form')->findFormByAlias($form);
        
        if(!$formObject)
            throw $this->createNotFoundException();
        
        if(!$formObject->getTemplate())
            $template = $this->getDoctrine()->getRepository('PiZoneFormBundle:Template')->findTemplate();
        else
            $template = $formObject->getTemplate();
        
        if(!$template)
            throw $this->createNotFoundException();
        
        $filename = md5($template->getCss() . date("D M j G:i:s T Y"). $this->get('pi_zone.form_processing')->randomStr(10)).'.css';
        $filePath = $this->getParameter('pi_zone_form.uploads.files.absolute_path') . $filename;
        $this->get('pi_zone.form_processing')->writeToFile($filePath, $template->getCss());
        
        $response = new BinaryFileResponse($filePath);
        $response->headers->set('Content-Type', 'text/css');
        $response->headers->set('Content-Disposition', 'attachment;filename="'.$filename);
        $response->headers->set('Content-Disposition', 'attachment;filename="'.$filename);
        
        return  $response;
    }
    
    private function getFormFiles($form){
        $result = array();
        $files = $this->get('request_stack')->getCurrentRequest()->files;
        $files = $files->get($form);
        if($files){
            foreach($files['more_fields'] as $key => $one){
                $result[] = array(
                    'key' => $key,
                    'origin_name' => $one->getClientOriginalName(),
                    'extention' => $one->getExtension(),
                    'file' => $one);
                unset($this->formResult['more_fields'][$key]);
            }
        }
        
        return $result;
    }
    
    public function getDefaultTemplateAction(Request $request) {
        $fields = $request->get('fields');
        $callback = $request->get('callback');
        
        $form = new Form();
        $moreFields = array();;
        if($fields)
            $moreFields = $fields;
        $template = $this->get('pi_zone.form_generator')->compileTemplate($form, $moreFields);
        
        $defaultTemplate = $this->getDoctrine()->getManager('default')->getRepository('PiZoneFormBundle:Template')->findTemplate();
        $css = $defaultTemplate->getCss();
        
        $response = new JsonResponse(array('template' => $template, 'css' => $css), 200, array());
        if($callback)
            $response->setCallback($callback);
        return $response;
    }
}
