<?php

namespace PiZone\FormBundle\Controller\Field;

use PiZone\AdminBundle\Controller\AEditController;
use PiZone\AdminBundle\Controller\IEditController;

/**
 * WebItem controller.
 *
 */
class EditController extends AEditController implements IEditController
{
    public function __construct(){
        $this->model = 'PiZone\FormBundle\Entity\Field';
        $this->form = 'PiZone\FormBundle\Form\FieldType';
        $this->routeList['update'] = 'form_field_update';
        $this->routeList['delete'] = 'form_field_delete';
    }
}