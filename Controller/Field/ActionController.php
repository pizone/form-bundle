<?php

namespace PiZone\FormBundle\Controller\Field;

use PiZone\AdminBundle\Controller\AActionController;

/**
 * WebItem controller.
 *
 */
class ActionController extends AActionController
{
    public function __construct(){
        $this->model = 'PiZone\FormBundle\Entity\Field';
        $this->repository = 'PiZone\FormBundle\Entity\Field';
        $this->route['delete'] = 'form_field_delete';
        $this->route['list']['name'] = 'form_field';
    }
}