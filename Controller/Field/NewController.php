<?php

namespace PiZone\FormBundle\Controller\Field;

use PiZone\AdminBundle\Controller\ANewController;
use PiZone\AdminBundle\Controller\INewController;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Util\Codes;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;

/**
 * WebItem controller.
 *
 */
class NewController extends ANewController implements INewController
{
    public function __construct(){
        $this->model = 'PiZone\FormBundle\Entity\Field';
        $this->form = 'PiZone\FormBundle\Form\FieldType';
        $this->route['create'] = 'form_field_create';
    }
}
