<?php

namespace PiZone\FormBundle\Controller\Event;

use Doctrine\Common\Collections\ArrayCollection;
use PiZone\AdminBundle\Controller\AListController;
use PiZone\AdminBundle\Controller\IListController;
use Symfony\Component\HttpFoundation\Request;

/**
 * User list controller.
 *
 */
class ListController  extends AListController implements IListController
{
    protected $userId = null;

    public function __construct(){
        $this->prefixSession = 'PiZone\FormBundle\FormEventList';
        $this->routeList = array(
            'list' => array(
                'name' => 'form_event',
                'parameters' => array()
            ),
            'delete' => 'form_event_delete',
            'batch' => array(
                'delete' => 'form_event_batch_delete',
                'active' => 'form_event_batch_active'
            )
        );
        $this->model = 'PiZone\FormBundle\Entity\Event';
        $this->repository = 'PiZone\FormBundle\Entity\Event';
        $this->filtersForm = 'PiZone\FormBundle\Form\EventFilterType';
    }
    
    protected function getlist($entities){
        $tokenManager = $this->get('security.csrf.token_manager');

        foreach ($entities as $one) {
            $tokenId = $this->generateUrl($this->routeList['delete'], array('id' => $one->getId()));
            $token = $tokenManager->getToken($tokenId);

            $this->fieldList[] = array(
                'id' => $one->getId(),
                'title' => $one->getTitle(),
                'alias' => $one->getAlias(),
                'description' => $one->getDescription(),
                'subject' => $one->getSubject(),
                'content' => $one->getContent(),
                'is_active' => $one->getIsActive(),
                '_token' => $token->getValue()
            );
        }

        return $this->fieldList;
    }


    protected function processFilters($query)
    {
        $filterObject = $this->getFilters();

        $queryFilter = $this->getQueryFilter();
        $queryFilter->setQuery($query);


        if (isset($filterObject['all']) && null !== $filterObject['all']) {
            $queryFilter->addOrStringFilter('title', $filterObject['all']);
            $queryFilter->addOrStringFilter('alias', $filterObject['all']);
            $queryFilter->addOrStringFilter('description', $filterObject['all']);
            $queryFilter->addOrStringFilter('subject', $filterObject['all']);
            $queryFilter->addOrStringFilter('content', $filterObject['all']);
        }
        if (isset($filterObject['title']) && null !== $filterObject['title']) {
            $queryFilter->addStringFilter('title', $filterObject['title']);
        }
        if (isset($filterObject['alias']) && null !== $filterObject['alias']) {
            $queryFilter->addStringFilter('alias', $filterObject['alias']);
        }
        if (isset($filterObject['description']) && null !== $filterObject['description']) {
            $queryFilter->addStringFilter('description', $filterObject['description']);
        }
        if (isset($filterObject['subject']) && null !== $filterObject['subject']) {
            $queryFilter->addStringFilter('subject', $filterObject['subject']);
        }
        if (isset($filterObject['content']) && null !== $filterObject['content']) {
            $queryFilter->addStringFilter('content', $filterObject['content']);
        }
        if (isset($filterObject['is_active']) && null !== $filterObject['is_active']) {
            $queryFilter->addBooleanFilter('is_active', $filterObject['is_active']);
        }
    }
}