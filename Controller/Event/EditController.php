<?php

namespace PiZone\FormBundle\Controller\Event;

use PiZone\AdminBundle\Controller\AEditController;
use PiZone\AdminBundle\Controller\IEditController;

/**
 * WebItem controller.
 *
 */
class EditController extends AEditController implements IEditController
{
    public function __construct(){
        $this->model = 'PiZone\FormBundle\Entity\Event';
        $this->form = 'PiZone\FormBundle\Form\EventType';
        $this->routeList['update'] = 'form_event_update';
        $this->routeList['delete'] = 'form_event_delete';
    }
}