<?php

namespace PiZone\FormBundle\Controller\Event;

use PiZone\AdminBundle\Controller\AActionController;

/**
 * WebItem controller.
 *
 */
class ActionController extends AActionController
{
    public function __construct(){
        $this->model = 'PiZone\FormBundle\Entity\Event';
        $this->repository = 'PiZone\FormBundle\Entity\Event';
        $this->route['delete'] = 'form_event_delete';
        $this->route['list']['name'] = 'form_event';
    }
}