<?php

namespace PiZone\FormBundle\Controller\Event;

use PiZone\AdminBundle\Controller\ANewController;
use PiZone\AdminBundle\Controller\INewController;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Util\Codes;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;

/**
 * WebItem controller.
 *
 */
class NewController extends ANewController implements INewController
{
    public function __construct(){
        $this->model = 'PiZone\FormBundle\Entity\Event';
        $this->form = 'PiZone\FormBundle\Form\EventType';
        $this->route['create'] = 'form_event_create';
    }
}
