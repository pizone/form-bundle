<?php

namespace PiZone\FormBundle\Controller\Form;

use PiZone\AdminBundle\Controller\ANewController;
use PiZone\AdminBundle\Controller\INewController;
use PiZone\FormBundle\Entity\Form;
use PiZone\FormBundle\Entity\FormEvent;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Util\Codes;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;

/**
 * WebItem controller.
 *
 */
class NewController extends ANewController implements INewController
{
    public function __construct(){
        $this->model = 'PiZone\FormBundle\Entity\Form';
        $this->form = 'PiZone\FormBundle\Form\FormType';
        $this->route['create'] = 'form_form_create';
    }

    protected function getObject() {
        $form = new Form();

        $event = $this->getDoctrine()->getRepository('PiZoneFormBundle:Event')->findOneBy(array('alias' => 'message_to_user'));

        $eventSendUser = new FormEvent();
        $eventSendUser->setEvent($event);
        $form->addFormEvent($eventSendUser);

        $event = $this->getDoctrine()->getRepository('PiZoneFormBundle:Event')->findOneBy(array('alias' => 'message_to_admin'));

        $eventSendUser = new FormEvent();
        $eventSendUser->setEvent($event);
        $form->addFormEvent($eventSendUser);

        return $form;
    }

    protected function save($entity, $form){
        $em = $this->getDoctrine()->getManager($this->manager);
        $entity->addUser($this->getUser());
        $em->persist($entity);
        $em->flush();

        $template = null;
        foreach ($entity->getFormTemplates() as $one) {
            if ($one->isSelected()) {
                $template = $one;
            }
        }
        $entity->setTemplate($template);
        $em->persist($entity);
        $em->flush();
    }
}
