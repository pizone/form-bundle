<?php

namespace PiZone\FormBundle\Controller\Form;

use PiZone\AdminBundle\Controller\AActionController;

/**
 * WebItem controller.
 *
 */
class ActionController extends AActionController
{
    public function __construct(){
        $this->model = 'PiZone\FormBundle\Entity\Form';
        $this->repository = 'PiZone\FormBundle\Entity\Form';
        $this->route['delete'] = 'form_form_delete';
        $this->route['list']['name'] = 'form_form';
    }
}