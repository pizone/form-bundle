<?php

namespace PiZone\FormBundle\Controller\Form;

use Doctrine\Common\Collections\Criteria;
use PiZone\AdminBundle\Controller\AEditController;
use PiZone\AdminBundle\Controller\IEditController;
use PiZone\FormBundle\Entity\FormEvent;

/**
 * WebItem controller.
 *
 */
class EditController extends AEditController implements IEditController
{
    protected $originalFields = array();
    protected $originalTemplates = array();

    public function __construct(){
        $this->model = 'PiZone\FormBundle\Entity\Form';
        $this->form = 'PiZone\FormBundle\Form\FormType';
        $this->routeList['update'] = 'form_form_update';
        $this->routeList['delete'] = 'form_form_delete';
    }

    protected function getObject($em, $pk) {
        $form = $this->getQueryBuilder($em, $pk)->getOneOrNullResult();
        if ($form) {
            $eventsCollection = $form->getFormEvent();

            $event = $em->getRepository('PiZoneFormBundle:Event')->findOneBy(array('alias' => 'message_to_user'));
            $criteria = Criteria::create()
                ->where(Criteria::expr()->eq("event", $event));

            $formEvent = $eventsCollection->matching($criteria);
            if ($formEvent->count() == 0) {
                $eventSendUser = new FormEvent();
                $eventSendUser->setEvent($event);
                $form->addFormEvent($eventSendUser);
            }


            $event = $em->getRepository('PiZoneFormBundle:Event')->findOneBy(array('alias' => 'message_to_admin'));
            $criteria = Criteria::create()
                ->where(Criteria::expr()->eq("event", $event));

            $formEvent = $eventsCollection->matching($criteria);
            if ($formEvent->count() == 0) {
                $eventSendUser = new FormEvent();
                $eventSendUser->setEvent($event);
                $form->addFormEvent($eventSendUser);
            }


            $template = $form->getTemplate();
            if($template){
                $templatesCollection = $form->getFormTemplates();
                $criteria = Criteria::create()
                    ->where(Criteria::expr()->eq("id", $template->getId()));
                $formTemplate = $templatesCollection->matching($criteria);
                $selected = $formTemplate->first();
                $selected->setIsSelected(1);
            }
        }
        return $form;
    }

    protected function getQueryBuilder($em, $pk) {
        return $em
            ->getRepository('PiZone\FormBundle\Entity\Form')
            ->createQueryBuilder('q')
            ->addSelect('f')
            ->leftJoin('q.form_fields', 'f')
            ->where('q.id = :pk')
            ->setParameter(':pk', $pk)
            ->orderBy('f.sort')
            ->getQuery();
    }

    public function preBindRequest($Form) {
        foreach ($Form->getFormFields() as $field) {
            $this->originalFields[] = $field;
        }

        foreach ($Form->getFormTemplates() as $templates) {
            $this->originalTemplates[] = $templates;
        }
    }

    public function preSave($entity, $form) {
        foreach ($entity->getFormFields() as $field) {
            foreach ($this->originalFields as $key => $toDel) {
                if ($toDel->getId() === $field->getId()) {
                    unset($this->originalFields[$key]);
                }
            }
        }

        foreach ($entity->getFormTemplates() as $templates) {
            foreach ($this->originalTemplates as $key => $toDel) {
                if ($toDel->getId() === $templates->getId()) {
                    unset($this->originalTemplates[$key]);
                }
            }
        }
    }

    protected function save($em, $entity, $form){
        foreach ($this->originalFields as $field) {
            $em->remove($field);
        }
        foreach ($this->originalTemplates as $templates) {
            $em->remove($templates);
        }
        $em->persist($entity);
        $em->flush();

        $template = null;
        foreach($entity->getFormTemplates() as $one){
            if($one->isSelected()){
                $template = $one;
            }
        }

        $entity->setTemplate($template);
        $em->persist($entity);
        $em->flush();
    }
}