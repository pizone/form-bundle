<?php

namespace PiZone\FormBundle\Controller\Form;

use Doctrine\Common\Collections\ArrayCollection;
use PiZone\AdminBundle\Controller\AListController;
use PiZone\AdminBundle\Controller\IListController;
use Symfony\Component\HttpFoundation\Request;

/**
 * User list controller.
 *
 */
class ListController  extends AListController implements IListController
{
    protected $userId = null;

    public function __construct(){
        $this->prefixSession = 'PiZone\FormBundle\FormFormList';
        $this->routeList = array(
            'list' => array(
                'name' => 'form_form',
                'parameters' => array()
            ),
            'delete' => 'form_form_delete',
            'batch' => array(
                'delete' => 'form_form_batch_delete',
                'active' => 'form_form_batch_active'
            )
        );
        $this->model = 'PiZone\FormBundle\Entity\Form';
        $this->repository = 'PiZone\FormBundle\Entity\Form';
        $this->filtersForm = 'PiZone\FormBundle\Form\FormFilterType';
    }
    
    protected function getlist($entities){
        $tokenManager = $this->get('security.csrf.token_manager');

        foreach ($entities as $one) {
            $tokenId = $this->generateUrl($this->routeList['delete'], array('id' => $one->getId()));
            $token = $tokenManager->getToken($tokenId);

            $this->fieldList[] = array(
                'id' => $one->getId(),
                'title' => $one->getTitle(),
                'alias' => $one->getAlias(),
                'description' => $one->getDescription(),
                'is_active' => $one->getIsActive(),
                '_token' => $token->getValue()
            );
        }

        return $this->fieldList;
    }


    protected function processFilters($query)
    {
        $filterObject = $this->getFilters();

        $queryFilter = $this->getQueryFilter();
        $queryFilter->setQuery($query);


        if (isset($filterObject['all']) && null !== $filterObject['all']) {
            $queryFilter->addOrStringFilter('title', $filterObject['all']);
            $queryFilter->addOrStringFilter('alias', $filterObject['all']);
            $queryFilter->addOrStringFilter('description', $filterObject['all']);
        }
        if (isset($filterObject['title']) && null !== $filterObject['title']) {
            $queryFilter->addStringFilter('title', $filterObject['title']);
        }
        if (isset($filterObject['alias']) && null !== $filterObject['alias']) {
            $queryFilter->addStringFilter('alias', $filterObject['alias']);
        }
        if (isset($filterObject['description']) && null !== $filterObject['description']) {
            $queryFilter->addStringFilter('description', $filterObject['description']);
        }
        if (isset($filterObject['is_active']) && null !== $filterObject['is_active']) {
            $queryFilter->addBooleanFilter('is_active', $filterObject['is_active']);
        }
    }
}