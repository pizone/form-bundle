<?php

namespace PiZone\FormBundle\Controller\Status;

use PiZone\AdminBundle\Controller\AActionController;

/**
 * WebItem controller.
 *
 */
class ActionController extends AActionController
{
    public function __construct(){
        $this->model = 'PiZone\FormBundle\Entity\Status';
        $this->repository = 'PiZone\FormBundle\Entity\Status';
        $this->route['delete'] = 'form_status_delete';
        $this->route['list']['name'] = 'form_status';
    }
}