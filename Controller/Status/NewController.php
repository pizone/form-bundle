<?php

namespace PiZone\FormBundle\Controller\Status;

use PiZone\AdminBundle\Controller\ANewController;
use PiZone\AdminBundle\Controller\INewController;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Util\Codes;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;

/**
 * WebItem controller.
 *
 */
class NewController extends ANewController implements INewController
{
    public function __construct(){
        $this->model = 'PiZone\FormBundle\Entity\Status';
        $this->form = 'PiZone\FormBundle\Form\StatusType';
        $this->route['create'] = 'form_status_create';
    }
}
