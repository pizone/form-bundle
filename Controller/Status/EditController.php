<?php

namespace PiZone\FormBundle\Controller\Status;

use PiZone\AdminBundle\Controller\AEditController;
use PiZone\AdminBundle\Controller\IEditController;

/**
 * WebItem controller.
 *
 */
class EditController extends AEditController implements IEditController
{
    public function __construct(){
        $this->model = 'PiZone\FormBundle\Entity\Status';
        $this->form = 'PiZone\FormBundle\Form\StatusType';
        $this->routeList['update'] = 'form_status_update';
        $this->routeList['delete'] = 'form_status_delete';
    }
}